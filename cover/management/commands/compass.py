import fnmatch
import glob
import os
import subprocess
import re
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = re.sub(r'[\s\n]+',  ' ', """
        A compass compiler command to work around having to pack sass/compass for heroku.
        The compiled files MUST be updated and checked in every time (hence "work around").
    """).strip()

    def handle(self, *args, **options):
        self.verbosity = int(options['verbosity'])
        self.command = 'compile'
        for line in subprocess.check_output("grep -r --include='*.html' -o -E '\\{% static .*css. *%}' . ", shell=True).split('\n'):
            match = re.search(r':.*?(["\'])(.*?)\1', line)
            if match:
                compile_target = match.group(2)
                found_targets = glob.glob('*/static/' + compile_target)
                if found_targets:
                    if len(found_targets) > 1:
                        raise CommandError("Found multiple possible targets for %s: %r" % (compile_target, found_targets))
                    target = re.sub(r'\.css$', '.scss', found_targets[0])
                    if os.path.exists(target):
                        self.log("Found compile target %s" % target)
                        static_dir = re.match(r'.*?/static/', target).group()
                        command = "compass %(cmd)s %(scss_file)s -s compressed --css-dir=%(path)s --sass-dir=%(path)s --image-dir=%(image_dir)s %(includes)s" % dict(
                            cmd=self.command,
                            scss_file=target,
                            path=os.path.dirname(target),
                            image_dir=static_dir + "images",
                            includes=' '.join('-I ' + path for path in glob.glob('*/static/'))
                        )
                        self.log("Running> %s" % command)
                        os.system(command)

    def log(self, msg):
        if self.verbosity > 0:
            self.stdout.write(msg)