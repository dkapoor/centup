from django.conf.urls import patterns, include, url
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views import defaults

# These sometimes need to render in frames:
defaults.page_not_found = xframe_options_exempt(defaults.page_not_found)
defaults.server_error = xframe_options_exempt(defaults.server_error)
defaults.permission_denied = xframe_options_exempt(defaults.permission_denied)


urlpatterns = patterns('cover.views',
    url(r'^$', 'home', name='home'),
    url(r'^about/?$', 'about', name='about'),
    url(r'^terms/?$', 'terms', name='terms'),
    url(r'^faqs/?$', 'faqs', name='faqs'),
    url(r'^privacy_policy/?$', 'privacy_policy', name='privacy_policy'),
    url(r'^publisher_benefits/?$', 'publisher_benefits', name='publisher_benefits'),
    url(r'^charity_benefits/?$', 'charity_benefits', name='charity_benefits'),
    url(r'^community_benefits/?$', 'community_benefits', name='community_benefits'),
    url(r'^request_invite_code/?$', 'request_invite_code', name='request_invite_code'),
    url(r'^request_invite_code_success/?$', 'request_invite_code_success', name='request_invite_code_success'),
    url(r'^request_invite_code_failure/?$', 'request_invite_code_failure', name='request_invite_code_failure'),
)