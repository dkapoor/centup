jQuery(function(){
    $('textarea.code.snippet').focus(function() {
        var el = this;
        setTimeout(function(){
            el.select();
        },1);
    })


    $('[data-update-dollars]').change(updateDollars).each(updateDollars);

    function updateDollars() {
        var input = $(this);
        var value = Number(input.val());
        $(input.data("update-dollars")).html( "<span class='dollars'>"  + value.toFixed(2)*100 +
        "</span>" +  "<br>" + "<span class='small'>" + "$" + value.toFixed(2) + "</span>");
    }

    $('[data-update-credits]').change(updateCredits).each(updateCredits);

    function updateCredits() {
        var input = $(this);
        var value = Number(input.val());
        $(input.data("update-credits")).html(value);
    }

    $("button.ajax").click( function(ev){
        ev.preventDefault();

        var el = $(this);
        $.ajax({
            url: el.attr('formaction'),
            type: el.attr('formmethod'),
            headers: {
                'X-CSRFToken': window._csrftoken
            },
            complete: function complete(xhr, status) {
                return el.trigger('ajax:complete', [xhr, status]);
            },
        });
    });


    $(".charity button.selector").on('ajax:complete', function(ev, xhr, status){
        if (status === 'success') {
            $(".charity button.selector.selected").removeClass("selected");
            $(this).addClass('selected');
            console.log('yah hi')
        } else {
            console.error(status, xhr.responseText)
        };
    })
})
