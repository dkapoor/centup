from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from publishers.models import Publisher, URL
from charities.models import Charity
from mailchimp import utils

MAILCHIMP_LIST_ID = 'd8ef093bd4' # DRY :)
REDIRECT_URL_NAME = '/request_invite_code_success/'

def home(request):
    if request.user.is_authenticated():
        if request.user.is_publisher:
            return HttpResponseRedirect(
                reverse('users:dash'))
        else:
            return HttpResponseRedirect(reverse('publishers:landing'))
    else:
        try:
                email_address = request.POST['email']
                if request.method == 'POST':
                    list = utils.get_connection().get_list_by_id(MAILCHIMP_LIST_ID)
                    list.subscribe(email_address, {'EMAIL': email_address})
                    return HttpResponseRedirect('/request_invite_code_success/')
                else:
                    return HttpResponseRedirect('/request_invite_code_failure/')

        except:
                if request.method == 'GET':
                    return render(request, 'home.html', dict(
                        publishers=Publisher.objects.all(),
                        charities=Charity.objects.all(),
                        featured_publishers=Publisher.objects.filter(approved=True, featured=True)[:12],
                        contents=URL.objects.filter(total_donations_hc__gt=1).extra(order_by = ['-total_donations_hc'])[:10]
                        
                  
        ))

@login_required
def about(request):
    return render(request, 'about.html')

def terms(request):
    return render(request, 'terms.html')

def faqs(request):
    return render(request, 'faqs.html')

def privacy_policy(request):
    return render(request, 'privacy_policy.html')

def publisher_benefits(request):
    return render(request, 'publisher_benefits.html')

def charity_benefits(request):
    return render(request, 'charity_benefits.html')

def community_benefits(request):
    return render(request, 'community_benefits.html')

#This view is hacked together quickly with terrible error handling. Just needed to get somethinng out the door that works. 
def request_invite_code(request):
    try:
        email_address = request.POST['email']
        if request.method == 'POST':
            list = utils.get_connection().get_list_by_id(MAILCHIMP_LIST_ID)
            list.subscribe(email_address, {'EMAIL': email_address})
            return HttpResponseRedirect('/request_invite_code_success/')
        else:
            return HttpResponseRedirect('/request_invite_code_failure/')

    except:
        if request.method == 'GET':
            return render(request, 'request_invite_code.html')
        else:
            return HttpResponseRedirect('/request_invite_code_failure/')    

   
def request_invite_code_failure(request):
    return render(request, 'request_invite_code_failure.html')  

def request_invite_code_success(request):
    return render(request, 'request_invite_code_success.html')  

