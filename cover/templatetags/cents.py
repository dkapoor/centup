from django.contrib.humanize.templatetags import humanize
from django.template import Library
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

import utils.credits

register = Library()


@register.filter(needs_autoescape=True)
def credits(value, autoescape=None):
    if autoescape:
        value = conditional_escape(value)
    return mark_safe(humanize.intcomma(int(float(value))))

@register.filter(needs_autoescape=True)
def dollars(value, autoescape=None):
    if autoescape:
        value = conditional_escape(value)
    return mark_safe("$%.2f" % utils.credits.to_real_dollars(int(float(value))))


@register.filter(needs_autoescape=True)
def abbrev_credits(value, autoescape=None):
    if autoescape:
        value = conditional_escape(value)
    cent_value = int(float(value))
    if cent_value < 9999:
        formatted = humanize.intcomma(cent_value)
    if cent_value >= 10000:
         formatted = "%sK" % humanize.intcomma(cent_value/1000)   
    else:
        formatted = humanize.intcomma(cent_value)
    return mark_safe(formatted)

 