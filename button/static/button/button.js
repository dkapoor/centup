var agent   = navigator.userAgent;

    if ( agent.match(/(iPhone|iPod|iPad|Blackberry|Android)/) ) {
        // Mobile controls. Needs separate handling.
        jQuery(function($){
            function showCentup(response) {
                $("#balance").fadeOut(300, function(){
                    $("#balance").addClass('incr');
                    $("#balance").html("+" + response.amount);
                    $("#balance").fadeIn(300, function() {
                        $("#balance").fadeOut(500, function() {
                            $("#balance").removeClass('incr');
                            $("#balance").html(response.balance);
                            $("#balance").fadeIn(300);
                        })
                    });
                });
            }


            window.showCentup = showCentup;
            $("#container").click(function() {
                var el = $(this);
                //removed the cleaner handoff for ios
                $.ajax({
                    url: el.data('posthref'),
                    type: 'POST',
                    data: {
                        url: el.data('url'),
                        'X-CSRFToken': el.data('csrftoken'),
                    },
                    headers: {
                        'X-CSRFToken': el.data('csrftoken'), 
                    },
                    complete: function complete(xhr, status) {
                        var response = $.parseJSON(xhr.responseText);
                        if( response.action === 'success') {
                            showCentup(response);
                            // Need to figure out a way around this click function, but it works
                            $("#container").click(function() {
                            var centupPopup = window.open(
                                        'popup/success/' +
                                        '?donation=' +
                                        response.donation,
                                        'centupPopup',
                                        'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                                        'resizable=no,copyhistory=no,height=340,width=470,'+
                                        'left='+ (screen.width / 2 - (470/2) ) +
                                        ',top='+ (screen.height / 2 - (340/2) )
                                    )
                            });

                        } else {
                            //have to add the jquery css call again
                            $("#container").click(function() {
                            var el = $(this);    
                            var centupPopup = window.open(
                                        'popup/error/',
                                        'centupPopup',
                                        'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                                        'resizable=no,copyhistory=no,height=340,width=470,'+
                                        'left='+ (screen.width / 2 - (470/2) ) +
                                        ',top='+ (screen.height / 2 - (340/2) )
                                    )
                            centupPopup.location = el.data('errorhref')
                            window._centupErrorInfo = {
                                message: response.error,
                                title: response.title,
                                url: response.url,
                                urlText: response.actionable,
                                hackyExtra: response.hackyExtra
                            };
                            
                            
                            });
                             
                            

                        }
                        if( status !== 'success') {
                            console.error(xhr.responseText)
                        }
                    },
                });
            })
        });
    }
/*For browsers outside of iOS*/
    else {
        jQuery(function($){
            function showCentup(response) {
                $("#balance").fadeOut(300, function(){
                    $("#balance").addClass('incr');
                    $("#balance").html("+" + response.amount);
                    $("#balance").fadeIn(300, function() {
                        $("#balance").fadeOut(500, function() {
                            $("#balance").removeClass('incr');
                            $("#balance").html(response.balance);
                            $("#balance").fadeIn(300);
                        })
                    });
                });
            }


            window.showCentup = showCentup;
            $("#container").click(function() {
                var el = $(this);
                var centupPopup = window.open(
                            el.data('loadinghref'),
                            'centupPopup',
                            'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                            'resizable=no,copyhistory=no,height=340,width=470,'+
                            'left='+ (screen.width / 2 - (470/2) ) +
                            ',top='+ (screen.height / 2 - (340/2) )
                        )
                $.ajax({
                    url: el.data('posthref'),
                    type: 'POST',
                    data: {
                        url: el.data('url'),
                        'X-CSRFToken': el.data('csrftoken'),
                    },
                    headers: {
                        'X-CSRFToken': el.data('csrftoken'), 
                    },
                    complete: function complete(xhr, status) {
                        var response = $.parseJSON(xhr.responseText);
                        if( response.action === 'success') {
                            showCentup(response);
                            var centupPopup = window.open(
                                        'popup/success/' +
                                        '?donation=' +
                                        response.donation,
                                        'centupPopup',
                                        'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                                        'resizable=no,copyhistory=no,height=340,width=470,'+
                                        'left='+ (screen.width / 2 - (470/2) ) +
                                        ',top='+ (screen.height / 2 - (340/2) )
                                    ) 
                        } 

                        else {

                        //Created a demo call. need to fix no demo site
                        if( response.action === 'demo') {
                            showCentup(response);
                            var centupPopup = window.open(
                                        'popup/demo/' +
                                        '?donation=' +
                                        response.donation,
                                        'centupPopup',
                                        'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                                        'resizable=no,copyhistory=no,height=340,width=470,'+
                                        'left='+ (screen.width / 2 - (470/2) ) +
                                        ',top='+ (screen.height / 2 - (340/2) )
                                    ) 
                        } 

                        else {
                            var centupPopup = window.open(
                                        'popup/error/',
                                        'centupPopup',
                                        'toolbar=no,location=no,directories=no,status=no,menubar=no,'+
                                        'resizable=no,copyhistory=no,height=340,width=470,'+
                                        'left='+ (screen.width / 2 - (470/2) ) +
                                        ',top='+ (screen.height / 2 - (340/2) )
                                    )
                            centupPopup.location = el.data('errorhref')
                            window._centupErrorInfo = {
                                message: response.error,
                                title: response.title,
                                url: response.url,
                                urlText: response.actionable,
                                hackyExtra: response.hackyExtra
                            };

                        }
                        }
                        if( status !== 'success') {
                            console.error(xhr.responseText)
                        }
                    },
                });
            })
        });

    }


