
(function() {
    // We should always be the last script tag in the document
    // at this point since we should have just finished executing..
    var scripts = document.getElementsByTagName("script");
    var url = scripts[scripts.length-1].src.match(
        /(^.*)\/button\.js$/)[1] + '/button/iframe';
    var buttons = document.getElementsByClassName("centup");

    for (var i = 0; i < buttons.length; i++) {
        var btn = buttons[i];

        btn.innerHTML = '<iframe ' +
            'style="border:none;width:150px;height:36px;" ' +
            'id="centup_iframe"' +
            'src="' + url +
            '?url=' + encodeURIComponent(
                btn.getAttribute("data-url") || window.location.href) +
            '&title=' + encodeURIComponent(
                btn.getAttribute("data-title") || document.title) +
            '"></iframe>';
    };
})();


//when the browser is firefox, grab only iframes with the centup_iframe id 
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
if (isFirefox)
    {
        //fix for the iframe caching issue in firefox and in general
        Object.prototype.each = function(f){
            var l = this.length;
            for(var i=0;i<l;i++)
                {
                    try {
                        this[i].eachCall = f;
                        this[i].eachCall(this[i]);
                    } catch(e){ throw e; }
                }
        };
        document.getElementsByTagName("centup_iframe").location.reload();

    }   



