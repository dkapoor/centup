import logging
logger = logging.getLogger(__name__)
from urlparse import urlparse

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.http import require_POST
from django.views.decorators.clickjacking import xframe_options_exempt

from accounts.models import Donation
from cover.templatetags import cents
from publishers.models import Publisher, URL

@login_required
def button_code(request):
    """Button code instructions for inclusion in publisher site"""

    return render(request, 'button/code.html', dict(
        script='<script src="%s%s"></script>' % (
                settings.PUBLIC_URL_ROOT,
                reverse('button:script')
            ),
        placeholder='<div class="centup"></div>',
        url_placeholder='<div class="centup" '
            'data-url="http://www.example.com/article/123" '
            'data-title="Article Title Here">'
            '</div>',
        tumblr_placeholder='<div class="centup" data-url="{Permalink}" data-title="{Title}"></div>'    
    ))

def button_script(request):
    """
    Render javascript for SCRIPT-tag included in
    publisher sites.

    This is actually a static file, but we want to show a "canonical"
    URL that won't change should the AWS url need to move around - so,
    for now, a simple redirect.
    """

    url = staticfiles_storage.url("button.js")

    return HttpResponsePermanentRedirect(url)


@xframe_options_exempt
def button_iframe(request):
    """Render button IFRAME container"""
    is_error_page = request.GET.get('error_page', False)
    try:
        url = URL.from_string(
            request.GET.get('url',
                request.META.get('HTTP_REFERER', None)
            ), title=request.GET.get('title', ''))
        if url is None:
            raise Exception("You must provide a URL for the CentUp button.")
        response =  render(request, 'button/button_frame.html', dict(
            url=url,
            problem=Donation.verify_donation(
                    user=request.user,
                    url=url,
                    amount="default"
                )
        ))
        if is_error_page:
            return HttpResponse(
                "Hmm... It seems that the error you were looking for has gone away.",
                content_type = 'text/plain'
            )

        else:
            return response

    except Exception, e:
        # Handle rendering of errors differently
        if is_error_page:
            raise
        error = "%s: %s" % (e.__class__, str(e))
        logger.exception(error)
        return render(request, 'button/button_error.html', dict(error=error))


@login_required
def success_popup(request):
    """Render post-donation success popup"""
    try:
        donation = get_object_or_404(Donation, pk=request.GET['donation'])
        if donation.source_account.user != request.user:
            raise PermissionDenied("Donation does not originate form this account")
        return  render(request, 'button/success.html', dict(
            max_donation=request.user.max_donation,
            amount=cents.credits(donation.amount),
            balance=cents.abbrev_credits(donation.url.total_donations),
            url=donation.url.url,
            title=donation.url.title,
            publisher=donation.publisher_account.publisher,
            charity=donation.charity_account.charity,
            acct_balance=cents.credits(request.user.account.balance),
        ))
    except Exception, e:
        error = "%s: %s" % (e.__class__, e)
        logger.exception(error)
        return render(request, 'button/popup_problem.html',
            dict(problem=error))

@require_POST
@login_required
def extra_donation(request):
    try:
        donation = Donation.register(
                user=request.user,
                url=request.POST['url'],
                amount=int(request.POST['extra_donation'])
            )
        return render(request, 'button/thanks.html', dict(
                action='extra',
                amount=cents.credits(donation.amount),
                balance=cents.abbrev_credits(donation.url.total_donations),
                charity=donation.charity_account.charity,
                publisher=donation.publisher_account.publisher,
                long_url=donation.url.url,
                title=donation.url.title
            ))
    except Exception, e:
        error = "%s: %s" % (e.__class__, e)
        logger.exception(error)
        return render(request, 'button/popup_problem.html',
            dict(problem=error))

@xframe_options_exempt
def popup_demo(request):
    return render(request, 'button/popup_demo.html')

@xframe_options_exempt
def error_popup(request):
    return render(request, 'button/popup_problem.html')


