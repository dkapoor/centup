from django.conf.urls import patterns, include, url
from  django.views.defaults import page_not_found

urlpatterns = patterns('button.views',
    url(r'^\.js$', 'button_script', name='script'),
    url(r'^/iframe/?$', 'button_iframe', name='iframe'),
    url(r'^/popup/$', page_not_found, name='popup_base'),
    url(r'^/popup/success/?$', 'success_popup', name='success_popup'),
    url(r'^/popup/extra/?$', 'extra_donation', name='extra_donation'),
    url(r'^/popup/error/?$', 'error_popup', name='error_popup'),
    url(r'^/popup/demo/?$', 'popup_demo', name='popup_demo'),
    url(r'^/code$', 'button_code', name='code'),
)