centup
======

Installation
------------

*Note:* `centup` cannot run on Windows, at least not the easy heroku way.

Begin by cloning the project:

    git clone git@github.com:centup/centup.git
    cd centup

For local development, you must have [`Python 2.7.3 `](http://www.python.org/download/releases/2.7.3/) installed. Once installed, also install pip - usually you can do this using:

    easy_install-2.7 install pip

In *nix, your distro probably has it as a package instead (`sudo apt-get install python-pip`),

You will also need [`Postgresql 9.1`](http://www.postgresql.org/download/). Create a an empty database as well as a user with access to it. You can do so using  [pgAdmin](http://www.pgadmin.org/) or, through the command line (using `centup` as both the datbase and user name):

    sudo -u postgres createuser -sP centup
    sudo -u postgres createdb -O centup centup

You can now install all python dependencies. You may experience less headaches installing bindings if you can download speicifc packages for your OS for `PIL` (Python Imaging Library) and `psycopg2` (the postgresql bindings). For example, in Ubuntu:

    sudo apt-get install python-imaging python-psycopg2

Either way, ensure all python dependencies are satisfied by running:

    sudo pip install -r requirements.txt

Finally, [install the heorku toolkit](https://toolbelt.heroku.com/) and log in as instructed.

### Configuration ###

Production configuration options are stored exclusively in heroku (using [`heroku config`](https://devcenter.heroku.com/articles/config-vars)). You can set-up your own local configuration by making a `.env` file.

The following configuration variables are available:

#### `DATABASE_URL` (required) ####

A db connection string with the format `postgres://username:password@host:port/database-name`. For example:

    DATABASE_URL=postgres://centup:centup@localhost:5432/centup

#### `FACEBOOK_APP_ID` (required) ####

The Facebook App ID to use for login. You *must* [set up your own facebook app](https://developers.facebook.com/apps) for login to work, as any app is tied to exactly one redirect URL domain. When doing so, enable sandbox mode and don't bother setting up a heroku URL or App Domains. Click the checkmark next to "Website with Facebook Login" and enter the URL you'll be using (probably `https://localhost:5000/`)

#### `FACEBOOK_API_SECRET` (required) ####

The secret key for the above. It is listed in Facebook's app page just below the App ID.

#### `PRODUCTION` ####

Defaults to `False`.

If set, enables Django's debug behavior (stack traces in error pages and so forth), as well as S3 for static file storage. Because of this the following `AWS_*` settings must be configured when `PRODUCTION` is set.

#### `AWS_ACCESS_KEY_ID` ####

Amazon S3 access key id for static files (if `PRODUCTION` is on).

#### `AWS_SECRET_ACCESS_KEY` ####

Amazon S3 access key secret for static files (if `PRODUCTION` is on).

#### `AWS_STORAGE_BUCKET_NAME` ####

Defaults to `centup`.

Amazon S3 bucket name used for static files (if `PRODUCTION` is on).

#### `PUBLIC_URL_ROOT` ####

Defaults to `http://centup.herokuapp.com`.

URL root when building absolute URLs (notably, in emails and for publishing the button script URL).

#### `BRAINTREE_MERCHANT_ID` (required) ####

Braintree Merchant ID (available from the braintree dashboard).

#### `BRAINTREE_PUBLIC_KEY` (required) ####

Braintree Public Key (available from the braintree dashboard).

#### `BRAINTREE_PRIVATE_KEY` (required) ####

Braintree Private Key (available from the braintree dashboard).

#### `SENDGRID_USERNAME` ####

Set this to enable sendgrid for email-sending. Otherwise all emails will just be sent to `stdout` instead.

#### `SENDGRID_PASSWORD` ####

Required when `SENDGRID_USERNAME` is set.


Braintree Private Key (available from the braintree dashboard).

### Database Setup ###

The following command will set up the requisite schema:

    foreman run python manage.py syncdb

You will be prompted to create a superuser that can be used to access the admin site.

You should re-run this command whenever the schema changes. Note that this won't work in the case of destructive changes (`syncdb` is conservative about such things). For now just clear your database when this happens. Later on, we will be using `south` to manage db migrations.

### Running ###

If everything went well you can simply run using:

    foreman start

For development purposes, you can also take advantage of auto-reloading and the wonderful [Werkzeug debugger](http://werkzeug.pocoo.org/docs/debug/) (it provides an in-browser debugger via AJAX and magic) by instead using:

    foreman start -f devel

The Bloody Compass Compiler
---------------------------

As a result of no small amount of hair pulling getting the `sass` compiler packaged for Heroku,
a less-then-ideal solution is required to compile the `scss` files to `css`.

Whenever you change an scss file, you must run:

    foreman run python manage.py compass

You must make sure to check in *both* the `css` and `scss` changes to git. You should *never* edit the `css` files directly (your changes will be overwritten).

There is also [this thing](http://compass.handlino.com/) which can probably automated this for you until we get it sorted properly.


Applications
------------

Django applications encapsulate related modules of functionality and their corresponding views, models, templates etc.


### Cover ###

The cover app contains the general layout, the front page, static about pages (much like a magazine cover) and other centup-wide utilities like custom template tags

### Accounts ###

The accounts app handles monetary transactions and funding.

### Users ###

The users app handles the user record keeping as well as all authentication concerns. We are using `django.contrib.auth` as it is simple, well supported and easy to integrate with 3rd party auth plugins.

### Publishers ###

The publishers app handles publisher profiles, reporting and site management.

### Charities ###

The charity app handles charity profile info.

### Button ###

The button app is used for the external handling of the "CentUp" button.

### Utils ###

The utils app is the catch-all for non-centup-specific utilities and/or unpackaged 3rd party libraries or snippets. Avoid using if possible.

### Notifications ###

Email notification functionality driven by signals from other apps. E-mail templates are found in this app's template directory. The first line is used as the subject.
