from django.contrib import admin
from news.models import Post

class PostAdmin(admin.ModelAdmin):
    
    prepopulated_fields = {
        'slug': ('title',)
    }

admin.site.register(Post, PostAdmin)

