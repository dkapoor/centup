from django.conf.urls import patterns, include, url
#from news import views

urlpatterns = patterns('news.views',
    url(r'^/(?P<post_slug>[-_\w]+)/?$', 'post_detail', name='post'),
    url(r'^/$', 'post', name='posts'),
)

