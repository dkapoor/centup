from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=120)
    slug = models.SlugField(max_length=50, unique=True)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.title

