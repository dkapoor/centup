from datetime import datetime, timedelta

from django.shortcuts import render, get_object_or_404

from news.models import Post

def post(request):
    return render(request, 'post.html', dict(
        posts=Post.objects.all(),
    ))

def post_detail(request, post_slug):
    post = get_object_or_404(Post, slug=post_slug)
    return render(request, 'post_detail.html', {'post':post})
