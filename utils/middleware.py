import re

from django.http import HttpResponseRedirect

class ForceSSLMiddleware(object):
    def process_request(self, request):
        if not (request.is_secure() or
            request.META.get("HTTP_X_FORWARDED_PROTO", "") == 'https'):

            url = request.build_absolute_uri(request.get_full_path())
            secure_url = re.sub(r'^http://', "https://", url)
            return HttpResponseRedirect(secure_url)