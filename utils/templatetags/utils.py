from django.template import Library, Node, resolve_variable, TemplateSyntaxError, Variable
from django.db.models.loading import get_model

register = Library()

class AddParameter(Node):
    def __init__(self, varname, value):
        self.varname = Variable(varname)
        self.value = Variable(value)

    def render(self, context):
        req = Variable('request').resolve(context)
        params = req.GET.copy()
        params[self.varname.resolve(context)] = self.value.resolve(context)
        return '%s?%s' % (req.path, params.urlencode())

@register.tag
def addurlparameter(parser, token):
    from re import split
    bits = split(r'\s+', token.contents, 2)
    if len(bits) < 2:
        raise TemplateSyntaxError, "'%s' tag requires two arguments" % bits[0]
    return AddParameter(bits[1],bits[2])

@register.filter
def model_lookup(value, model_name):
    if value:
        app, model = model_name.split(".")
        return get_model(app, model).objects.get(pk=value)