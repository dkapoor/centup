from cover.templatetags.cents import credits, dollars

class AccountBalanceMixin(object):
    def credit_balance(self, obj):
        return credits(obj.account.balance)

    def dollar_balance(self, obj):
        return dollars(obj.account.balance)