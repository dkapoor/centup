import decimal

from django.conf import settings


def from_decimal(dec):
    with decimal.localcontext() as ctx:
        ctx.traps[decimal.Inexact] = 1
        return int((dec * 200).to_integral_exact())

def to_real_dollars(credits):
    return (credits * settings.CENTUP_FEE_FACTOR) / 100