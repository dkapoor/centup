from django import forms
class FieldPlaceholderMixin(object):
    """
    Add to a form to have the widgets emit their label in the
    `placeholder` attribute, their `help_text` in `title`, and
    `required` into `required`.
    """
    def __init__(self, *args, **kwargs):
        super(FieldPlaceholderMixin, self).__init__(*args, **kwargs)
        for field in self.fields.itervalues():
            field.widget.attrs['placeholder'] = field.label
            if field.help_text:
                field.widget.attrs['title'] = field.help_text
            if field.required:
                field.widget.attrs['required'] = 'required'

class HalfCreditField(forms.IntegerField):
    """Accepts integer amounts and converst them to half-credits"""
    def to_python(self, value):
        # value is what comes in as a string (full credits)
        # return what is used as python (half-credits)
        return super(HalfCreditField, self).to_python(value) * 2

    def prepare_value(self, value):
        # value is orginal python value (half credits)
        # return what to render (full credits)
        pyval = super(HalfCreditField, self).prepare_value(value)
        if pyval is None:
            return None
        else:
            return int(pyval) / 2