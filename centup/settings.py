from os import environ as env
import os.path
from django.conf import global_settings as defaults


from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.request',
)


# Core settings:

DEBUG = not env.get('PRODUCTION', False)
TEMPLATE_DEBUG = DEBUG

INSTALLED_APPS = (
    #admin tools - commented these out because the database wont build on heroku
    #'admin_tools',
    #'admin_tools.theming',
    #'admin_tools.menu',
    #'admin_tools.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
   
    #other apps
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'postmonkey',
    'social_auth',
    'storages',
    'cover',
    'accounts',
    'publishers',
    'charities',
    'button',
    'users',
    'utils',
    'news',
    'notifications',
    'invites',
    'south',
    'endless_pagination',
    
)



if DEBUG:
    INSTALLED_APPS += ('django_extensions',)


AUTH_USER_MODEL = 'users.User'

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

TEMPLATE_CONTEXT_PROCESSORS = defaults.TEMPLATE_CONTEXT_PROCESSORS + (
    'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_login_redirect',
    'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware', # /!\ XTRA IMPORTANT FOR CENTUP /!\
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'users.middleware.SocialAuthExceptionForwarding',
)

if env.get('FORCE_SSL', False):
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    MIDDLEWARE_CLASSES += ('utils.middleware.ForceSSLMiddleware',)
    SOCIAL_AUTH_REDIRECT_IS_HTTPS = True

# CentUp-specific settings:

PUBLIC_URL_ROOT = env.get('PUBLIC_URL_ROOT', 'https://centup.herokuapp.com')

CENTUP_FEE_FACTOR = 0.9

MAX_MONETARY_DIGITS = 19

LOW_BALANCE_WARNING_THRESHOLD = 100

# Mailchimp 

MAILCHIMP_API_KEY = '615d274bcc7d1c0092c9e3f8743010f6-us6'
MAILCHIMP_LIST_ID = 'd8ef093bd4'
MAILCHIMP_WEBHOOKS_KEY = 'centup.org'

# django-social-auth settings:

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.facebook.FacebookBackend',
) + defaults.AUTHENTICATION_BACKENDS

FACEBOOK_APP_ID = env['FACEBOOK_APP_ID']
FACEBOOK_API_SECRET = env['FACEBOOK_API_SECRET']

FACEBOOK_EXTENDED_PERMISSIONS = ['email']

SOCIAL_AUTH_USER_MODEL = AUTH_USER_MODEL

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.user.get_username',
    'users.pipeline.check_username',
    'social_auth.backends.pipeline.misc.save_status_to_session',
#   'users.pipeline.invite_user',
        # taken care of by above
    'social_auth.backends.pipeline.user.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
    # 'users.pipeline.additional_details',
    'users.pipeline.download_avatar'
)

# Login URLs used by bothy contirb.auth and django-social-auth
# Note: Cannot use named routes (as per Django 1.5) as django-social-auth
# is not yet aware of them (as of 0.7.10)
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/login/?error=yes'

SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/welcome/'

# django-storages settings:
if not DEBUG:
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

    AWS_ACCESS_KEY_ID = env.get('AWS_ACCESS_KEY_ID', 'TODO: SHARED ID')

    AWS_SECRET_ACCESS_KEY = env['AWS_SECRET_ACCESS_KEY']

    AWS_STORAGE_BUCKET_NAME = env.get('AWS_STORAGE_BUCKET_NAME', 'centup')

    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

# braintree settings:

import braintree


if env.get('BRAINTREE_REAL_MONEY_BE_CAREFUL', False):
    bt_env = braintree.Environment.Production
else:
    bt_env = braintree.Environment.Sandbox

braintree.Configuration.configure(
    bt_env,
    merchant_id=env.get('BRAINTREE_MERCHANT_ID', 'TODO: SHARED BR SANDBOX'),
    public_key=env.get('BRAINTREE_PUBLIC_KEY', 'TODO: SHARED BR SANDBOX'),
    private_key=env.get('BRAINTREE_PRIVATE_KEY', 'TODO: SHARED BR SANDBOX'),
)

# email config for sendgrid
if 'SENDGRID_USERNAME' in env:
    EMAIL_HOST = 'smtp.sendgrid.net'
    EMAIL_HOST_USER = env['SENDGRID_USERNAME']
    EMAIL_HOST_PASSWORD = env['SENDGRID_PASSWORD']
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
else:
    # send email to console if no sendgrid credentials found
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

NOTIFACTION_FROM_ADDRESS = "CentUp <team@centup.org>"


# More core django settings:

import dj_database_url
DATABASES = {
    'default': dj_database_url.config()
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True


TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), '../template_overrides'),
)

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
if DEBUG:
    STATIC_URL = '/static/'
else:
    STATIC_URL = 'https://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
if DEBUG:
    MEDIA_ROOT = os.path.join(os.path.dirname(__file__), '../_devstatic/')
    # Serve media from staticfiles+
    STATICFILES_DIRS = (MEDIA_ROOT,)
else:
    MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
if DEBUG:
    MEDIA_URL = '/media/'
else:
    MEDIA_URL=''


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '1^pv&amp;zk5m8p^$%be+wb^2#&amp;@78-62@_*v4=s0tgwfk3peo_#5u'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)


ROOT_URLCONF = 'centup.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'centup.wsgi.application'


# Replace django logging hell with a dead simple heroku-friendly setup:
#   - by default, everything goes to stdout (where heroku will catch it, and
#     potentially forward it to an addon log service)
#   - especially chatty loggers can be disabled by setting them to the null
#     handler and turning off propagation
#   - in debug mode, a legible human formatter is used, elsewhere, a key-value
#     pair formatter (heroku's router uses this format as well)

import sys
LOGGING = {
    'version': 1,
    # Discard existing madness
    'disable_existing_loggers': True,
    'formatters': {
        # A console-friendly format, good for reading, bad for parsing
        'human': {
            'format': "%(levelname)s %(name)s: %(message)s"
        },
        # key-value pair format, good for parsing
        'kvpair': {
            'format': "at=%(levelname)s logger=%(name)s message=%(message)s"
        },
        # special case for recording accounting data
        'accounting': {
            'format': "at=%(levelname)s logger=%(name)s account=%(account)s op=%(op)s amount=%(amount)s"
        }
    },
    'filters': {
        'production_only': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'debug_only': {
            '()': 'django.utils.log.RequireDebugTrue'
        },
    },
    'handlers': {
        # discard
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        # handler for heroku log drains
        # production only, INFO+, kv-pair format
        'drain': {
            'level': 'INFO',
            'formatter': 'kvpair',
            'filters': ['production_only'],
            'class':'logging.StreamHandler',
            'stream': sys.stdout
        },
        # Special handler formatter for accounting information
        'transactions': {
            'level': 'INFO',
            'formatter': 'accounting',
            'filters': ['production_only'],
            'class':'logging.StreamHandler',
            'stream': sys.stdout
        },
        'console': {
            'level':'DEBUG',
            'formatter': 'human',
            'filters': ['debug_only'],
            'class':'logging.StreamHandler',
            'stream': sys.stdout
        }
    },
    'loggers': {
        # This is the catch-all logger - we want everything to go here
        # unless otherwise specified
        '': {
            'handlers': ['console', 'drain'],
            'level': 'DEBUG'
        },
        # Set this to 'console' to re-enable sql logging in debug mode
        'django.db.backends': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'south': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },
        # For some inconcievable reason, the django.* loggers won't
        # propagate to the root handler unless we specifically mention
        # them here
        'django': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': True
        },
        # The special accounting logger
        'accounts.accounting': {
            'handlers': ['console', 'transactions'],
            'level': 'INFO',
            'propagate': False
        }
    }
}
