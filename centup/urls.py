from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

# This effectly forces the admin site's login to just use the same
# login mechanics as everyone else
admin.site.login = login_required(admin.site.login)

admin.autodiscover()

publisher_patterns =patterns('')
invite_patterns =patterns('')

urlpatterns = patterns('',
    url(r'', include('cover.urls')),
    #url(r'^admintools/', include('admin_tools.urls')),

    url(r'^login/?$', 'django.contrib.auth.views.login', {
        'template_name': 'login.html'
    }, 'login'),

    url(r'^invite/check/?$', 'invites.views.enter_invite_code', name=
        'enter_invite_code'),

    url(r'^signup/?$', 'users.views.basic_info', name='signup'),

    url(r'^logout/?$', 'django.contrib.auth.views.logout', {
        'next_page': '/'
    }, 'logout'),

    url(r'^password/change/?$', 'django.contrib.auth.views.password_change', {
        'template_name': 'password/change.html',
        'post_change_redirect': '/user/dash/'
    }, 'password_change'),

    url(r'^password/reset/?$', 'django.contrib.auth.views.password_reset', {
        'template_name': 'password/reset.html',
    }, 'password_reset'),

    url(r'^password/reset/done/?$', 'django.contrib.auth.views.password_reset_done', {
        'template_name': 'password/done.html',
    }),
    url(r'^password/reset/confirm/(?P<uidb36>[\w\d]+)/(?P<token>[-\w\d]+)/?$', 'django.contrib.auth.views.password_reset_confirm', {
        'template_name': 'password/confirm.html',
    }),
    url(r'^password/reset/complete/?$', 'django.contrib.auth.views.password_reset_complete', {
        'template_name': 'password/complete.html',
    }),

    url(r'^welcome/?$', 'users.views.basic_info', name="welcome"),
    url(r'^welcome/1/?$', 'users.views.charity_selection', name="welcome1"),
    url(r'^welcome/2/?$', 'users.views.giving_options', name="welcome2"),
    url(r'^welcome/3/?$', 'users.views.payment_details', name="welcome3"),

    url(r'^user/', include('users.urls', namespace='users')),
    url(r'^button', include('button.urls', namespace='button')),

    url(r'^publisher', include('publishers.urls')),
    url(r'^charity', include('charities.urls')),

    url(r'^news', include('news.urls')),

    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'', include('social_auth.urls')),
    url(r'^invites/', include('invites.urls', namespace='invites')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),


    url(r'^favicon\.ico$',
     RedirectView.as_view(url=staticfiles_storage.url("favicon.ico").replace("%","%%"))),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += staticfiles_urlpatterns(prefix='/media/')