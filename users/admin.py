from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.http import HttpResponse

import braintree

from users.forms import UserCreationForm, UserChangeForm
from users.models import User
from utils.admin import AccountBalanceMixin

BT_LINK = '<a href="https://www.braintreegateway.com/merchants/%(merchant)s/payment_methods/%(token)s">%(token)s</a>'

class UserAdmin(UserAdmin, AccountBalanceMixin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_admin', 'braintree_vault', 'credit_balance', 'dollar_balance')
    list_filter = ('is_admin','username', 'email', 'first_name', 'last_name',)
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        ('Name', {'fields': ('first_name', 'last_name', 'large_avatar')}),
        ('Permissions', {'fields': ('is_admin',)}),
        ('Important dates', {'fields': ('last_login',)}),
        ('CentUp Settings', {'fields': ('preferred_charity', 'default_donation_hc', 'braintree_vault_token', 'auto_reload')}),

    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email', 'username')
    ordering = ('created_at',)
    filter_horizontal = ()

    def braintree_vault(self, obj):
        if obj.braintree_vault_token:
            return BT_LINK % dict(
                merchant = braintree.configuration.Configuration.merchant_id,
                token = obj.braintree_vault_token
            )
        else:
            return "&mdash;"
    braintree_vault.allow_tags = True

    

# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's builtin permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)