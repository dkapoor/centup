import logging
logger = logging.getLogger(__name__)

import urllib
import urllib2

from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from users.models import User

AVATAR_URL = "http://graph.facebook.com/%s/picture?width=100&height=100"

def download_avatar(backend, user, uid, *args, **kwargs):
    if backend.name == 'facebook' and not user.large_avatar:
        # urllib and django and all the IO interfaces are bloody stupid for not being able
        # to just stream .read()s from an http IO source - we have to read the file content's
        # in-memory like barbarians :(
        image = ContentFile(urllib2.urlopen(AVATAR_URL % uid).read())
        user.large_avatar.save("%s.jpg" % uid, image)

def additional_details(details, user, *args, **kwargs):
    user.username = user['username']


def check_username(details, *args, **kwargs):
    """and by username we actually mean email"""
    try:
        email = details['email']
        if User.objects.filter(email=email).exclude(password='!').exclude(password='').exists():
            # you already registered dum-dum!
            return HttpResponseRedirect("%s?%s" % (
                    reverse('login'), urllib.urlencode([('taken', email)])
                ))
    except:
        logger.exception("check_username problem")
        raise

def invite_user(request, user, username, details, *args, **kw):
    if not user:
        code = request.session.get('saved_code', None)
        if not code:
            return HttpResponseRedirect(reverse('enter_invite_code'))
    #'social_auth.backends.pipeline.user.create_user', more or less
    if user:
        return {'user': user}
    if not username:
        return
    return {
        'user': User.objects.create(
            username=username,
            email=details.get('email'),
            invite_id=code,
        ),
        'is_new': True
    }

