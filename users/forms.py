"""
User forms are used in the following places:

- sign up page, after only entering email (user won't exist yet)
- confirmation page after facebook auth (user will exist and be pre-populated from facebook)
- user dashboard for editing details
    > needs to link to pw change and facebook linking
- change / add password form
- forget password form
- the admin user creation and edit forms (passworded users only)

There is also a somewhat related payment info form in accounts.forms. This will
work off the braintree data store directly and never be saved on our end.

"""

from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from users.models import User
from utils.forms import FieldPlaceholderMixin, HalfCreditField
from utils.widgets import ClearableImageFileInput, EmailInput, SliderInput

from mailchimp import utils

class PasswordForm(forms.ModelForm):
    """User creation form for admin use"""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        #Check to see if password is long enough    
        if len(password1) < 6:
            raise forms.ValidationError('Password too short')
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(PasswordForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class UserCreationForm(PasswordForm):
    terms_and_conditions = forms.BooleanField(
        error_messages={'required': 'You must accept the Terms & Conditions.'},
        label="Terms & Conditions"
        )
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')
        widgets = {
            'invite': forms.TextInput
        }

class UserChangeForm(forms.ModelForm):
    """Edit form for the admin"""
    password = ReadOnlyPasswordHashField()
    default_donation_hc = HalfCreditField(
        label= "Default Donation",
    )
    terms_and_conditions = forms.BooleanField(
            error_messages={'required': 'You must accept the Terms & Conditions.'},
            label="Terms & Conditions"
        )
#This is governing the signup
    class Meta:
        model = User
        widgets = {
            'invite': forms.TextInput
        }

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class SocialAuthUserForm(FieldPlaceholderMixin, forms.ModelForm):
    """Confirm details after facebook auth"""
    terms_and_conditions = forms.BooleanField(
        error_messages={'required': 'You must accept the Terms & Conditions'},
        label="Terms & Conditions"
        )

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'username', 'large_avatar')
        widgets = {
            'large_avatar': forms.FileInput(attrs={'class' : 'file'}),
            'email': EmailInput,
        }

class PasswordSignUpForm(PasswordForm, SocialAuthUserForm):
    """Sign up without facebook"""

    class Meta(SocialAuthUserForm.Meta):
        fields = SocialAuthUserForm.Meta.fields 
        widgets = dict(SocialAuthUserForm.Meta.widgets.items() + [('invite', forms.TextInput)])

    def clean_invite(self):
        invite = self.cleaned_data['invite']
        if invite.uses < 1:
            raise forms.ValidationError("   invitation code has been used up.")

        return invite

    def clean(self):
        cleaned_data = super(SocialAuthUserForm, self).clean()
        invite = cleaned_data.get('invite')
        email = cleaned_data.get('email')

        if invite and email and invite.email and invite.email != email:

            self._errors["invite"] = self.error_class([
                "This invitation code is not valid for this email address."])
            del cleaned_data["invite"]

        # Always return the full collection of cleaned data.
        return cleaned_data



class UserCharitySelectionForm(forms.ModelForm):
    """Signup step 1: charity selection"""

    class Meta:
        model = User
        fields = ('preferred_charity',)
        widgets = {
            'preferred_charity': forms.RadioSelect
        }

class UserDonationSettingsForm(forms.ModelForm):
    """Signup step 2: donation settings
        - also, the "giving" tab on the user dash
    """

    default_donation_hc = HalfCreditField(
        label= "Default Donation",
        widget=SliderInput(attrs={
            'min':1,
            'max':100,
            'step':0,
            'data-update-credits': '#default_donation_credits'
        })
    )

    class Meta:
        model = User
        fields = ('preload_amount', 'auto_reload', 'default_donation_hc')
        widgets = {
            'preload_amount': SliderInput(attrs={
                'min':10.0,
                'max':100.0,
                'value':40.0,
                'step':1,
                'data-update-dollars': '#preload_amount_dollars',
            })
        }


class UserNotificationSettingsForm(forms.ModelForm):
    """For the dashboard"""

    class Meta:
        model = User
        fields =  (
            'charity_quote',
            'notify_on_low_balance',
            'notify_on_account_reload',
            'notify_on_charity_message',
            'periodic_updates',
            'public_profile',
        )



