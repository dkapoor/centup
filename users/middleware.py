from django.contrib.auth import REDIRECT_FIELD_NAME
from django.conf import settings


from social_auth.exceptions import AuthAlreadyAssociated
from social_auth.middleware import SocialAuthExceptionMiddleware


class SocialAuthExceptionForwarding(SocialAuthExceptionMiddleware):
    def raise_exception(self, request, exception):
        return False

    def get_message(self, request, exception):
        if isinstance(exception, AuthAlreadyAssociated):
            return """This %(backend)s account is already used from another CentUp account.
            If you would like to use your %(backend)s account, simply log out of CentUp,
            then log back in using %(backend)s.
            """ % dict(backend = request.social_auth_backend.AUTH_BACKEND.name)
        return super(SocialAuthExceptionForwarding, self).get_message(request, exception)

    def get_redirect_uri(self, request, exception):
        return request.session.get(REDIRECT_FIELD_NAME, settings.LOGIN_URL)
