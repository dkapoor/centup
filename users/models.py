from decimal import Decimal

from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, \
                                    BaseUserManager
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from social_auth.models import UserSocialAuth

from invites.models import InviteCode


class UserManager(BaseUserManager):
    def create_user(self, email, username=None, password=None, **other):
        user = self.model(email=email, username=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password,  **other):
        user = self.model(email=email, username=username)
        user.set_password(password)
        user.is_admin=True
        user.save()
        return user


class User(AbstractBaseUser):
    email = models.EmailField(max_length=254, unique=True, blank=False)
    is_active  = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)


    username  = models.CharField(max_length=254, blank=True,
        help_text="This is the name by which you will be seen publicly.")
    first_name   = models.CharField(max_length=128)
    last_name   = models.CharField(max_length=128)

    large_avatar = models.ImageField(upload_to='avatars/large/', blank=True,
        null=True, verbose_name='Avatar Image')

    account = models.OneToOneField('accounts.Account', related_name='user')

    preferred_charity = models.ForeignKey('charities.Charity', blank=True, null=True)
    default_donation_hc = models.PositiveIntegerField(default=50)

    preload_amount = models.DecimalField(
        default=Decimal('20.00'),
        blank=True,
        max_digits=settings.MAX_MONETARY_DIGITS,
        decimal_places=2
    )
    auto_reload = models.BooleanField(default=True)
    braintree_vault_token = models.CharField(max_length=255, blank=True, null=True)

    charity_quote = models.TextField(blank=True,
        verbose_name='Tell us why you support this charity ...',
        help_text="Your quote may be displayed on the charity's public profile page.")

    notify_on_low_balance = models.BooleanField(default=True,
        verbose_name="Notify me when my balance is low")
    notify_on_account_reload = models.BooleanField(default=True,
        verbose_name="Notify me when my account is reloaded")
    notify_on_charity_message = models.BooleanField(default=True,
        verbose_name="Notify me when a charity sends a message")
    periodic_updates = models.BooleanField(default=True,
        verbose_name="Send me periodic updates about CentUp")

    public_profile = models.BooleanField(default=True,
        verbose_name="Make my giving and content history public")

    invite = models.ForeignKey('invites.InviteCode', related_name='invited_users', null=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __unicode__(self):
        return self.get_full_name()

    objects = UserManager()

    @property
    def default_donation(self):
        if self.default_donation_hc % 2 != 0:
            raise ValueError("Default donation value can't be fractional!")
        return self.default_donation_hc / 2

    @default_donation.setter
    def default_donation(self, value):
        self.default_donation_hc = value * 2

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def has_password(self):
        return bool(self.password)

    @property
    def is_publisher(self):
        """For the moment this just means, has an approved publisher
        submitted"""
        return self.active_publishers.exists()

    @property
    def active_publishers(self):
        return self.publishers.filter(approved=True)

    def may_see_dashboard(self, publisher):
        return self.is_staff or \
            publisher in self.active_publishers

    @property
    def total_donations(self):
        return self._spent_cents_for_column('charity_amount_hc') + \
             self._spent_cents_for_column('publisher_amount_hc')

    def _spent_cents_for_column(self, col):
        return self.account.originated_donations.aggregate(
            amount_hc=models.Sum(col)
        )['amount_hc'] / 2.0


    MAX_DONATION = 1000
    @property
    def max_donation(self):
        if self.auto_reload:
            # When auto-reload is on, we can always donate whatever
            # we want!
            return self.MAX_DONATION
        else:
            return min(self.account.balance, self.MAX_DONATION)


