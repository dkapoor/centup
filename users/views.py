import urlparse

import braintree


from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from accounts.forms import BraintreeTransactionForm, BraintreePaymentUpdateForm
from publishers.models import Publisher
from charities.models import Charity
from users import forms
from news.models import Post
from utils import credits

from postmonkey import PostMonkey


pm = PostMonkey('615d274bcc7d1c0092c9e3f8743010f6-us6', timeout=10)



@login_required
def dash(request):
    donation_settings_form = forms.UserDonationSettingsForm(
        instance=request.user)
    notification_settings_form = forms.UserNotificationSettingsForm(
        instance=request.user)

    if request.method == 'POST':
        # We have several forms here, must distinguish between them
        # (the braintree form btw only comes here via GET string, dealt with
        # just below)
        formtype = request.POST.get('formtype', None)
        if formtype == 'donation_settings_form':
            donation_settings_form = forms.UserDonationSettingsForm(
                request.POST, instance=request.user)
            if donation_settings_form.is_valid():
                donation_settings_form.save()

        elif formtype == 'notification_settings_form':
            notification_settings_form = forms.UserNotificationSettingsForm(
                request.POST, instance=request.user)
            if notification_settings_form.is_valid():
                notification_settings_form.save()

        else:
            raise Exception('Need a valid formtype (got %r)' % formtype)


    update_payment_form = None
    if request.user.braintree_vault_token:
        query = request.META['QUERY_STRING']
        if 'page' in request.GET:
            # this is a pagination query string... disregard
            query = ""
        update_payment_form = BraintreePaymentUpdateForm(
            result=query,
            vault_token=request.user.braintree_vault_token,
            redirect_url=reverse('users:dash')
        )
        if update_payment_form.is_success:
            update_payment_form.update_payment_info(request.user)

    news=Post.objects.all()[:1]
    #Added a simple featured publisher
    return render(request, 'user/dash.html', dict(
        featured_publishers=Publisher.objects.filter(approved=True, featured=True)[:9],
        #hacky treatment to avoid displaying zero centup credits
        recent_centups=request.user.account.originated_donations.all().filter(publisher_amount_hc__gte=1).order_by(
            '-created_at')[:10],
        all_centups=request.user.account.originated_donations.all().order_by(
            '-created_at'),
        donation_settings_form=donation_settings_form,
        notification_settings_form=notification_settings_form,
        update_payment_form=update_payment_form,
        braintree_url=braintree.TransparentRedirect.url(),
        news=news,
    ))

def basic_info(request):
	
    if request.user.is_authenticated():
        form_class = forms.SocialAuthUserForm
        form_kwargs = dict(instance=request.user)
    else:
        form_class = forms.PasswordSignUpForm
        form_kwargs = {}

    #invite = request.GET.get('invite', None)
    #if invite:
    #    request.session['saved_code']=invite
    #    form_kwargs['initial'] = {'invite':invite}

    if request.method == 'POST':
        form = form_class(request.POST, request.FILES, **form_kwargs)
        if form.is_valid():
            #send an email address to mailchimp when someone signs up
            email_address = request.POST['email']
            pm.listSubscribe(id="d8ef093bd4", email_address=email_address)
            form.save()
            if not request.user.is_authenticated():
                # this is a new signup pw form
                new_user = authenticate(
                    username=form.cleaned_data['email'],
                    password=form.cleaned_data['password1'],
                )
                login(request, new_user)
            return HttpResponseRedirect(reverse('welcome1'))
    else:
        form = form_class(**form_kwargs)

    return render(request, 'user/basic_info.html', dict(
        form=form,
        publishers=Publisher.objects.filter(approved="TRUE").order_by('name'),
        charities=Charity.objects.filter(active="TRUE").order_by('name'),
        featured_publishers=Publisher.objects.filter(approved=True, featured=True)[:9]
    ))


@login_required
def charity_selection(request):
    if request.user.preferred_charity:
        # This user must be resuming their signup - just advance them
        # to the next step
        return HttpResponseRedirect(reverse('welcome2'))

    return render(request, 'user/charity_selection.html', dict(
        charities=Charity.objects.all(),
    ))

@login_required
def giving_options(request):
    if request.method == 'POST':
        form = forms.UserDonationSettingsForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('welcome3'))
    else:
        form = forms.UserDonationSettingsForm(instance=request.user)

    return render(request, 'user/giving_options.html', dict(
        form=form
    ))

@login_required
def payment_details(request):
    if request.user.braintree_vault_token:
        # This user already set their payment info, they can change
        # it in the dashboard
        return HttpResponseRedirect(reverse('users:dash'))
    try:
        form = BraintreeTransactionForm(request.META['QUERY_STRING'],
            redirect_url=reverse(payment_details),
            transaction_data={
                "type": "sale",
                "amount": request.user.preload_amount,
                "options": {
                    "submit_for_settlement": True,
                    "store_in_vault_on_success": True,
                    "add_billing_address_to_payment_method": True
                },
                "customer": {
                    "id": request.user.pk,
                    # The following are only being recorded to make
                    # manual reconciliation easier in braintree's
                    # command console
                    "first_name": request.user.first_name,
                    "last_name": request.user.last_name,
                    "email": request.user.email
                }
            })
        if form.is_success:
            form.authorize_funding(request.user)
            return HttpResponseRedirect(reverse('users:dash'))

        return render(request, 'user/payment_details.html', dict(
            form=form,
            braintree_url=braintree.TransparentRedirect.url()
        ))
    except braintree.exceptions.NotFoundError:
        # These almost always come from used up tokens - best to reset
        return HttpResponseRedirect(reverse(payment_details))