from django.db import models

from users.models import User

class Charity(models.Model):
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=50, unique=True)
    logo = models.ImageField(upload_to='logos/charities/', blank=True)
    cover = models.ImageField(upload_to='cover/charities/', blank=True)
    url = models.CharField(max_length=128, blank=True)
    account = models.OneToOneField('accounts.Account', related_name='charity')
    short_description = models.TextField(blank=True)
    description = models.TextField()
    contact_info = models.TextField(blank=True)
    featured=models.BooleanField(default=False)
    active=models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'charities'


    def top_donors(self, since=None):
        donations = self.account.charity_donations_sources
        donations = donations.values('source_account').annotate(earnings_hc=models.Sum('charity_amount_hc'))
        if since:
            donations = donations.filter(created_at__gte=since)
        return donations.order_by('-earnings_hc')

    def top_publishers(self, since=None):
        donations = self.account.charity_donations_sources
        donations = donations.values('publisher_account').annotate(earnings_hc=models.Sum('charity_amount_hc'))
        if since:
            donations = donations.filter(created_at__gte=since)
        return donations.order_by('-earnings_hc')

#added a proper supporter count by listing all users where preferred charity = self   
    def supporter_count(self):
        objects =  list(User.objects.filter(preferred_charity=self))
        if len(objects) == 0:
            return "0"
        else:
            return len(objects)

            


