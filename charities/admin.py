from django.contrib import admin

from charities import models
from utils.admin import AccountBalanceMixin

class CharityAdmin(admin.ModelAdmin, AccountBalanceMixin):
    exclude = ('account', )
    list_display = ('name', 'credit_balance', 'dollar_balance')

    prepopulated_fields = {
        'slug': ('name',)
    }

admin.site.register(models.Charity, CharityAdmin)
