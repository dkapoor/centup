from django.conf.urls import patterns, include, url

specific = patterns('charities.views',
    url(r'^dash/?$', 'dash', name='dash'),
    url(r'^$', 'detail', name='detail'),
    url(r'^preference/?$', 'preference', name='preference')
)

collection = patterns('charities.views',
    url(r'^/?$', 'index', name='index')
)


urlpatterns = patterns('s',
    url(r'^/(?P<slug>[-_\w]+)/', include(specific, namespace='charity')),
    url(r'^/$', include(collection, namespace='charities'))
)

