from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST

from charities.models import Charity
from users.models import User
from publishers.models import Publisher
from users import forms

DEFAULT_EARNERS_WINDOW=timedelta(days=90)

def dash(request, charity_slug):
    charity = get_object_or_404(Charity, pk=charity_id)
    data_since = datetime.now() - DEFAULT_EARNERS_WINDOW
    return render(request, 'charity/dash.html', dict(
        charity=charity,
        top_donors=_iter_top_donors(
            charity.top_donors(since=data_since)
        ),
        top_publishers=_iter_top_publishers(
            charity.top_publishers()
        )
    ))


def _iter_top_donors(qs):
    for donor in qs:
        yield User.objects.get(account=donor['source_account']), donor['earnings_hc'] / 2.0

def _iter_top_publishers(qs):
    for pub in qs:
        yield Publisher.objects.get(account=pub['publisher_account']), pub['earnings_hc'] / 2.0


def index(request):
    return render(request, 'charity/index.html', dict(
        charities=Charity.objects.all(),
    ))

@require_POST
@login_required
def preference(request, slug):
    charity = get_object_or_404(Charity, slug=slug)
    request.user.preferred_charity = charity;
    request.user.save()
    return HttpResponse('ok', 'text/plain')



def detail(request, slug):
    charity = get_object_or_404(Charity, slug=slug)
    data_since = datetime.now() - DEFAULT_EARNERS_WINDOW

    return render(request, 'charity/detail.html', dict(
        charity = charity,
        top_donors=_iter_top_donors(
            charity.top_donors(since=data_since)
        )
    ))
