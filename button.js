(function() {
    // We should always be the last script tag in the document
    // at this point since we should have just finished executing..
    var scripts = document.getElementsByTagName("script");
    var url = scripts[scripts.length-1].src.match(
        /(^.*)\/button\.js$/)[1] + '/button/iframe';
    var buttons = document.getElementsByClassName("centup");

    for (var i = 0; i < buttons.length; i++) {
        var btn = buttons[i];

        btn.innerHTML = '<iframe ' +
            'style="" ' +
            'src="' + url +
            '?url=' + encodeURIComponent(
                btn.getAttribute("data-url") || window.location.href) +
            '&title=' + encodeURIComponent(
                btn.getAttribute("data-title") || document.title) +
            '"></iframe>';
    };
})();
