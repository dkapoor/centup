# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        db.rename_table('accounts_centup', 'accounts_donation')


    def backwards(self, orm):
        db.rename_table('accounts_donation', 'accounts_centup')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'balance': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '19', 'decimal_places': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_fee_account': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'accounts.donation': {
            'Meta': {'object_name': 'Donation'},
            'charity_account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'charity_donations_sources'", 'to': u"orm['accounts.Account']"}),
            'charity_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fee_account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fee_centup_sources'", 'to': u"orm['accounts.Account']"}),
            'fee_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publisher_account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'publisher_donation_sources'", 'to': u"orm['accounts.Account']"}),
            'publisher_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'source_account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'originated_donations'", 'to': u"orm['accounts.Account']"}),
            'url': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['publishers.URL']"})
        },
        u'accounts.funding': {
            'Meta': {'object_name': 'Funding'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Account']"}),
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'braintree_txn_id': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'accounts.payout': {
            'Meta': {'object_name': 'PayOut'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['accounts.Account']"}),
            'balance': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'cheque_reference': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mailed_to': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'charities.charity': {
            'Meta': {'object_name': 'Charity'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'charity'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'contact_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'publishers.domain': {
            'Meta': {'object_name': 'Domain'},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'domains'", 'to': u"orm['publishers.Publisher']"})
        },
        u'publishers.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'publisher'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'paypal_email': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'registered_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']", 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'publishers.url': {
            'Meta': {'object_name': 'URL'},
            'centup_balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '19', 'decimal_places': '10'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'urls'", 'to': u"orm['publishers.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'user'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'auto_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'braintree_vault_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'charity_quote': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_donation': ('django.db.models.fields.DecimalField', [], {'default': "'0.25'", 'max_digits': '19', 'decimal_places': '10', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'notify_on_account_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_charity_message': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_low_balance': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'periodic_updates': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'preferred_charity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['charities.Charity']", 'null': 'True', 'blank': 'True'}),
            'preload_amount': ('django.db.models.fields.DecimalField', [], {'default': "'0.50'", 'max_digits': '19', 'decimal_places': '10', 'blank': 'True'}),
            'public_profile': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '254', 'blank': 'True'})
        }
    }

    complete_apps = ['accounts']