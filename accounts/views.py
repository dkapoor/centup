import logging
logger = logging.getLogger(__name__)

import braintree

from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import simplejson as json
from django.views.decorators.http import require_POST
from django.shortcuts import render, get_object_or_404

from accounts import models
from cover.templatetags import cents
from mailchimp import utils

@require_POST
def centup(request):
    """Process a CentUp button click"""

    try:
        problem = models.Donation.verify_donation(
            user=request.user,
            url=request.POST['url'],
            amount='default'
        )
        url=request.POST['url']
        if url == "http://localhost:5000":
            return HttpResponse(json.dumps(dict(
                action='demo',
                donation=0,
                amount=25,
                balance=25,
            )), content_type='application/json')
        if problem:
            return HttpResponse(json.dumps(dict(
                action='problem',
                url=problem['url'],
                title=problem['title'],
                error=problem['message'],
                actionable=problem['actionable'],
                hackyExtra=problem.get('hacky_extra', ''),
            )), content_type='application/json')

        donation = models.Donation.register(
            user=request.user,
            url=request.POST['url'],
        )
        return HttpResponse(json.dumps(dict(
            action='success',
            donation=donation.pk,
            amount=cents.credits(donation.amount),
            balance=cents.abbrev_credits(donation.url.total_donations)
        )), content_type='application/json')
    except Exception, e:
        error = "%s: %s" % (e.__class__, e)
        logger.exception(error)
        return HttpResponse(json.dumps(dict(
            action='error',
            error=error,
        )), content_type='application/json', status=500)
