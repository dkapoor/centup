from django.contrib import admin
from django.contrib import messages
from django.db import transaction
from django.utils.safestring import mark_safe

import braintree

from accounts import models, forms


BT_LINK = '<a href="https://www.braintreegateway.com/merchants/%(merchant)s/transactions/%(txn)s">%(txn)s</a>'

class WarningMixin(object):
    MESSAGE = mark_safe("""
        <strong>WARNING:</strong> This record is not meant to be
        manipulated directly. Account balances and other book-keeping
        data will <em>not</em> automatically update. Only use this
        to manually adjust a malformed record.
    """)
    def change_view(self, request, *args, **kw):
        messages.add_message(request, messages.WARNING, self.MESSAGE)
        return super(WarningMixin, self).change_view(
            request, *args, **kw)
    def add_view(self, request, *args, **kw):
        messages.add_message(request, messages.WARNING, self.MESSAGE)
        return super(WarningMixin, self).add_view(
            request, *args, **kw)
    def delete_view(self, request, *args, **kw):
        messages.add_message(request, messages.WARNING, self.MESSAGE)
        return super(WarningMixin, self).delete_view(
            request, *args, **kw)

class AccountAdmin(WarningMixin, admin.ModelAdmin):
    list_display = ('__unicode__', 'user_email_acct', 'balance')
    form = forms.AccountAdminForm

admin.site.register(models.Account, AccountAdmin)

class FundingAdmin(WarningMixin, admin.ModelAdmin):
    list_display = ('account', 'amount', 'braintree_transaction', 'created_at')

    def braintree_transaction(self, obj):
        return BT_LINK % dict(
            merchant = braintree.configuration.Configuration.merchant_id,
            txn = obj.braintree_txn_id
        )

    braintree_transaction.allow_tags = True


admin.site.register(models.Funding, FundingAdmin)

admin.site.register(models.PayOut)

class DonationAdmin(WarningMixin, admin.ModelAdmin):
    list_display = ('url', 'source_account', 'amount', 'created_at', 'publisher_account', 'publisher_amount', 'charity_account', 'charity_amount')
admin.site.register(models.Donation, DonationAdmin)


class CreditBonusAdmin(WarningMixin, admin.ModelAdmin):
    # don't warn for adding - adding is fine
    add_view = admin.ModelAdmin.add_view

    list_display = ('account', 'amount', 'issued_by', 'note', 'created_at')
    form = forms.CreditBonusAdminForm

    @transaction.commit_on_success
    def save_model(self, request, obj, form, change):
        obj.issued_by = request.user
        obj.save()
        if change:
            messages.add_message(request, messages.WARNING,
                "As this is not a new bonus, account balances have NOT been changed!")
        else:
            obj.account.add(obj.amount)

admin.site.register(models.CreditBonus, CreditBonusAdmin)