import braintree

from django import forms
from django.conf import settings

from accounts.models import Funding, Account, CreditBonus
from utils.forms import FieldPlaceholderMixin, HalfCreditField


class AccountAdminForm(forms.ModelForm):
    balance_hc = HalfCreditField(
        label="Balance",
        help_text="WARNING: Editing this value directly may interfere "
            "with CentUp's internal accounting, potentially leading "
            "to monetary loss! Only use for administrative "
            "adjustments!"
        )
    class Meta:
        model = Account

class CreditBonusAdminForm(forms.ModelForm):
    amount_hc = HalfCreditField(
        label="Bonus Amount (in credits)",
        help_text="Remember: 100 credits = $0.90"
    )
    class Meta:
        model = CreditBonus

class BraintreeForm(FieldPlaceholderMixin, forms.Form):
    tr_data = forms.CharField(widget=forms.HiddenInput)

    result = False

    def __init__(self, tr_data, *args, **kwargs):
        kwargs.setdefault('initial', {})
        kwargs['initial']['tr_data'] = tr_data

        if self.result and not self.is_success:
            data = dict(self._flat_params(self.result.params))
            data['tr_data'] = tr_data
            args = (data,) + args

        super(BraintreeForm, self).__init__(*args, **kwargs)

    @property
    def is_success(self):
        return self.result and self.result.is_success

    def clean(self):
        if self.result and not self.result.is_success:
            errors = self.result.errors.errors.data
            self._add_errors(self.result.errors.errors.data)
            if not self.errors:
                # result is a failure but no validation errors were built
                # - must be a gateway or processor error
                raise forms.ValidationError(self.result.message)
        return super(BraintreeForm, self).clean()

    def _add_errors(self, errors, base=()):
        for key, value in errors.iteritems():
            if key == 'errors':
                for error in value:
                    field_name = '__'.join(base + (error['attribute'],))
                    if field_name in self.fields:
                        error_field = field_name
                    else:
                        error_field = '__all__'
                    if error_field in self._errors:
                        self._errors[error_field].append(error['message'])
                    else:
                        self._errors[error_field] = self.error_class([error['message']])
            else:
                self._add_errors(value, base + (key,))


    def _flat_params(self, params):
        for key, value in params.iteritems():
            if isinstance(value, dict):
                for inner_key, value in self._flat_params(value):
                    yield "%s__%s" % (key, inner_key), value
            else:
                yield key, value


class BraintreeTransactionForm(BraintreeForm):
    """Non-model form for braintree integration. Set the action to the
    braintree TR url (normally `braintree.TransparentRedirect.url()`)

    - `result` should normally be request.META['QUERY_STRING']
    - `transaction_data` corresponds to tr_data["transaction] (this form is
    specifically for transaction operations)
    - `redirect_url` should be the response processor - to handle validation
    errors it should be the same view that rendered this form initially

    """
    transaction__credit_card__cardholder_name = forms.CharField(label="Name on Card")
    transaction__credit_card__number = forms.CharField(label="Card Number")
    transaction__credit_card__cvv = forms.CharField(label="Security Code (CVV)")
    transaction__credit_card__expiration_month = forms.CharField(label="MM")
    transaction__credit_card__expiration_year = forms.CharField(label="YYYY")
    #transaction__billing__street_address = forms.CharField(label="Street Address", required="False")
    #transaction__billing__extended_address = forms.CharField(label="Street Address 2", required="False")
    transaction__billing__postal_code = forms.CharField(label="Zip/Postal Code")

    def __init__(self, result, transaction_data, redirect_url, *args, **kwargs):
        if result:
            self.result = braintree.TransparentRedirect.confirm(result)

        tr_data = braintree.Transaction.tr_data_for_sale(
            {"transaction": transaction_data},
            settings.PUBLIC_URL_ROOT + redirect_url
        )
        super(BraintreeTransactionForm, self).__init__(tr_data, *args, **kwargs)

    def authorize_funding(self, user):
        funding = Funding.authorize(user.account, self.result.transaction)
        user.braintree_vault_token = \
                self.result.transaction.credit_card_details.token
        user.save()
        return funding


class BraintreePaymentUpdateForm(BraintreeForm):
    credit_card__cardholder_name = forms.CharField(label="Name on Card")
    credit_card__number = forms.CharField(label="Card Number")
    credit_card__expiration_month = forms.CharField(label="MM")
    credit_card__expiration_year = forms.CharField(label="YYYY")
    credit_card__cvv = forms.CharField(label="Security Code (CVV)")
    #credit_card__billing_address__street_address = forms.CharField(label="Street Address", required="False")
    #credit_card__billing_address__extended_address = forms.CharField(label="Street Address 2", required="False")
    credit_card__billing_address__postal_code = forms.CharField(label="Zip/Postal Code")

    def __init__(self, result, vault_token, redirect_url, *args, **kwargs):
        if result:
            self.result = braintree.TransparentRedirect.confirm(result)
            if self.is_success:
                vault_token = self.result.credit_card.token

        tr_data = braintree.CreditCard.tr_data_for_update(
            {
                "payment_method_token": vault_token,
                "credit_card": {
                    "billing_address": {
                        "options": {
                            "update_existing": True
                        }
                    }
                }
            },
            settings.PUBLIC_URL_ROOT + redirect_url
        )
        credit_card = braintree.CreditCard.find(vault_token)

        kwargs.setdefault('initial', {})
        kwargs['initial']['credit_card__cardholder_name'] = \
            credit_card.cardholder_name
        kwargs['initial']['credit_card__number'] = \
            credit_card.masked_number
        kwargs['initial']['credit_card__expiration_month'] = \
            credit_card.expiration_month
        kwargs['initial']['credit_card__expiration_year'] = \
            credit_card.expiration_year
        #kwargs['initial']['credit_card__billing_address__street_address'] = \
        #    credit_card.billing_address.street_address
        #kwargs['initial']['credit_card__billing_address__extended_address'] = \
        #    credit_card.billing_address.extended_address
        kwargs['initial']['credit_card__billing_address__postal_code'] = \
            credit_card.billing_address.postal_code


        super(BraintreePaymentUpdateForm, self).__init__(tr_data, *args, **kwargs)

        self.fields['tr_data'].widget = forms.HiddenInput()

    def update_payment_info(self, user):
        user.braintree_vault_token = self.result.credit_card.token