from django.dispatch import Signal

low_balance = Signal(providing_args=['balance'])

account_reload = Signal(providing_args=['balance', 'amount', 'datetime'])
