from datetime import datetime
import logging
logger = logging.getLogger(__name__)
accounting_logger = logging.getLogger('accounts.accounting')

import braintree

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db import transaction
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.urlresolvers import resolve

from accounts import signals
from charities.models import Charity
from publishers.models import Publisher, Domain, URL
from users.models import User
from utils import credits


def associate_account(instance, **kw):
    #urgh
    try:
        instance.account
    except Account.DoesNotExist:
        instance.account = Account.objects.create()

pre_save.connect(associate_account, sender=User)
pre_save.connect(associate_account, sender=Publisher)
pre_save.connect(associate_account, sender=Charity)


class Account(models.Model):
    balance_hc = models.PositiveIntegerField(default=0)

    @property
    def balance(self):
        return self.balance_hc / 2.0

    def add(self, amount):
        """ Add (debit) the account balance by the given
        amount of credits."""
        amount_hc = amount * 2
        self.__class__.objects.filter(pk=self.pk).update(
            balance_hc=models.F('balance_hc') + amount_hc
        )
        accounting_logger.info('added %s credits to %s', amount, self,
            extra=dict(op='add', amount=amount, account=self.pk))

    def deduct(self, amount):
        """ Deduct (credit) the account balance by the given
        amount of credits. Will raise IntegrityError if the balance would
        become negative."""
        amount_hc = amount * 2
        self.__class__.objects.filter(pk=self.pk).update(
            balance_hc=models.F('balance_hc') - amount_hc
        )
        new_balance = self.__class__.objects.get(pk=self.pk).balance
        accounting_logger.info('deducted %s credits from %s', amount, self,
            extra=dict(op='deduct', amount=amount, account=self.pk))
        if self.is_user_account() and new_balance < settings.LOW_BALANCE_WARNING_THRESHOLD < self.balance:
            signals.low_balance.send(sender=self, balance=new_balance)


    def is_user_account(self):
        try:
            return bool(self.user)
        except User.DoesNotExist:
            return False

#added simple model for admin view under the acct screen. This shoud only be used for the admin page.
    def user_email_acct(self):
        try:
            return str(self.user.email)
        except User.DoesNotExist:
            return False


    def __unicode__(self):
        objects =  list(User.objects.filter(account=self)) + \
            list(Publisher.objects.filter(account=self)) + \
            list(Charity.objects.filter(account=self))
        if len(objects) == 0:
            logger.warn("Orphaned account! (#%s)" % self.pk)
            return "<orphan Account>"
        elif len(objects) > 1:
            logger.warn("Conjoined account! (#%s)" % self.pk)
            return "<conjoined Account for  %s>" % ', '.join(map(str, objects))
        else:
            o = objects[0]
            return "<Account for %s %s (#%s)>" % (o.__class__.__name__, o, o.pk)



class Funding(models.Model):
    """A user funding their account using a braintree transaction"""
    account = models.ForeignKey(Account)
    braintree_txn_id = models.CharField(max_length=64)
    amount_hc = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def amount(self):
        return self.amount_hc / 2.0

    @classmethod
    @transaction.commit_on_success
    def authorize(cls, account, transaction):
        """`transaction` is a braintree transaction object, `account` is
        the account that will be funded"""
        funding = cls()
        funding.account = account
        funding.braintree_txn_id = transaction.id
        funding.amount_hc = credits.from_decimal(transaction.amount)
        funding.save()
        account.add(funding.amount)
        return funding

class CreditBonus(models.Model):
    """Free credits granted to users, not backed by money"""
    account = models.ForeignKey(Account)
    amount_hc = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    issued_by = models.ForeignKey('users.User',
        related_name="issued_bonuses", editable=False)
    note = models.TextField(null=True, blank=True,
        help_text="Administrative note describing why this credit was issued.")

    class Meta:
        verbose_name_plural = "Credit Bonuses"

    @property
    def amount(self):
        return self.amount_hc / 2.0


class PayOut(models.Model):
    account = models.ForeignKey(Account)
    mailed_to = models.CharField(max_length=256,
        help_text="The full address the cheque was sent to.")
    cheque_reference = models.CharField(max_length=256,
        help_text="The cheque serial number.")
    amount_hc = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

class Donation(models.Model):

    url = models.ForeignKey('publishers.URL')

    source_account = models.ForeignKey(Account,
        related_name='originated_donations')
    publisher_account = models.ForeignKey(Account,
        related_name='publisher_donation_sources')
    publisher_amount_hc = models.PositiveIntegerField()

    charity_account = models.ForeignKey(Account,
        related_name='charity_donations_sources')
    charity_amount_hc = models.PositiveIntegerField()

    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def amount(self):
        return self.publisher_amount + self.charity_amount

    @property
    def publisher_amount(self):
        return self.publisher_amount_hc / 2.0

    @property
    def charity_amount(self):
        return self.charity_amount_hc / 2.0

    @classmethod
    def verify_donation(cls, user, url, amount):
        """Verifies if a CentUp donation with the given parameters
        would go through. `amount` can be the string "default" to use
        the user's `default_donation` amount. Returns `None` is the
        donation would go through successfully, or a dictionary describing
        the reason it would not.

        The problem dictionary contains the following keys:

            `title`:        A brief description to render in the button itself
            `message`:      A longer description to show on hover
            `url`:          A url name for rectifying the problem
            'actionable':   Some actionable phrase which may act as the url text.
            'hacky_extra':  Some hacky extra code 

        """
        # raise error if button test on centup homepage. This is for handling the test button. 
        #if url == "http://localhost:5000":
        #    return dict(
        #        title="Just Testing",
        #        message="Just Testing! You need to login or signup to do a real transaction. Wouldn't it be realy fun to acually add some money?",
        #        actionable="Login",
        #        url=reverse('login'),
        #        hacky_extra='<br />or <a href="%s" target="_new">Sign Up &raquo;</a>' % reverse('signup')
        #    ) 


        if not user.is_authenticated():
            return dict(
                title="New to CentUp? Not Logged In?",
                message="Give a few cents to this content creator and a charity with the click of a button. Sounds amazing right?",
                actionable=0,
                url=0,
                # TODO: just make problems renderables - this is awful
                hacky_extra='<a class="btn-custom" href="%s" target="_new">Sign Up </a>' % reverse('signup')
            )
        
            
        if amount == "default":
            if not user.default_donation:
                return dict(
                    title="Account Problem",
                    message="Looks like you haven't configured your default "
                    "CentUp amount.",
                    actionable="Set Default",
                    url=reverse('users:dash')
                )
            amount = user.default_donation

        if user.account.balance < amount:
            if not user.auto_reload:
                return dict(
                    title="Account Problem",
                    message="Looks like you don't have enough funds left in your "
                    "account to make this donation.",
                    actionable="Add Funds",
                    url=reverse('users:dash')
                )
            if not user.braintree_vault_token:
                return dict(
                    title="Payment Problem",
                    message="Looks like you don't have enough funds in "
                    "your account to make this donation, or we don't have your "
                    "payment information.",
                    actionable="Go To My Account",
                    url=reverse('users:dash')
                )

        if not user.preferred_charity:
            return dict(
                title="Charity Problem",
                message="Looks like you haven't told us which charity to send part "
                "of your donation to.",
                actionable="Select Charity",
                url=reverse('users:dash')
            )
 


    class MissingCharity(Exception):
        pass

    @classmethod
    @transaction.commit_on_success
    def register(cls, user, url, title=None, amount=None):
        """Register a donation from the given user to the given
        URL string. The URL's domain will be looked by publisher.

        If the domain name cannot be ascertained from the URL
        `URL.MissingDomain` will be raised.

        If no publisher has registered that domain, then
        `URL.UnclaimedDomain` will be raised.

        The charity will be looked up by user. If the user has no
        `preferred_charity` set-up then `Donation.MissingCharity` is raised.

        If the user's balance would become negative from this transaction
        IntegrityError is raised.
        """

        url = URL.from_string(url)
        try:
            charity = user.preferred_charity
        except Charity.DoesNotExist:
            raise Donation.MissingCharity()

        publisher = url.domain.publisher

        if amount is None:
            amount =  user.default_donation

        if not isinstance(amount, int):
            raise TypeError('amount (%r) must be an integer' % amount)


        charity_amount = publisher_amount = (amount / 2.0)

        if user.auto_reload and user.account.balance < amount:
            result = braintree.Transaction.sale({
                "payment_method_token": user.braintree_vault_token,
                "amount": user.preload_amount,
                # "credit_card": {"cvv": "100"}
            })
            funding = Funding.authorize(user.account, result.transaction)
            signals.account_reload.send(sender=user,
                balance=user.account.balance + funding.amount,
                amount=funding.amount,
                datetime=datetime.now()
            )

        user.account.deduct(amount)
        publisher.account.add(publisher_amount)
        charity.account.add(charity_amount)

        url.register_donation_amount(amount)

        return cls.objects.create(
            url=url,
            source_account=user.account,
            publisher_account=publisher.account,
            publisher_amount_hc=publisher_amount * 2.0,
            charity_account=charity.account,
            charity_amount_hc=charity_amount * 2.0,
        )

