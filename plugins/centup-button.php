<?php
/*
Plugin Name: Centup Button
Version: 1.0
Description: This is a simple button that adds a CentUp button to the end of a blog post
Author: CentUp Industries
Author URI: http://www.centup.org
Plugin URI: http://www.centup.org/plugins/wordpress
License: Copyright 2013  CENTUP_INDISTRIES  (email : team@centup.org)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function add_centup_button($content) {
    if (is_home()) {
        return $content;
    }

    else {
            $content .= '<div class="centup" data-url="' . $permalink_post . '" data-title="' . $post_title . '"></div>';
            return $content;
        }


    
}
add_filter('the_content', 'add_centup_button');
 
function add_centup_js(){
    echo '<script src="https://www.centup.org/button.js"></script>';
}
add_action('wp_footer', 'add_centup_js');

?>