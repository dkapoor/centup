from django.contrib import admin

from invites import models, forms, signals

class InvitesAdmin(admin.ModelAdmin):
    list_display = ('code', 'uses', 'bonus_credit', 'note', 'email', 'issued_by', 'invited_users_count')

    form = forms.InviteCodeForm

    def invited_users_count(self, obj):
        return obj.invited_users.count()

    def save_model(self, request, obj, form, change):
        if not change:
            obj.issued_by = request.user
        obj.save()
        if form.cleaned_data['send_invitation_email']:
            signals.requested_invite_notifiction.send(sender=obj)

admin.site.register(models.InviteCode, InvitesAdmin)