import re

from django.core.validators import validate_email
from django import forms


from invites.models import InviteCode
from utils.forms import HalfCreditField

class InviteCodeForm(forms.ModelForm):
    bonus_credit_hc = HalfCreditField(
        label="Bonus Credit Amount",
        help_text="Signups from this invitation code will recive this "
            "many free credits. (100 credits = $1.00) This is affects real "
            "money, use with care.",
        initial=0
    )
    send_invitation_email = forms.BooleanField(required=False,
        help_text="Send an inviation email to this invite's address?")

    class Meta:
        model = InviteCode

class BulkInviteForm(forms.Form):
    bonus_credit_hc = HalfCreditField(
        label="Bonus Credit Amount",
        help_text="All invitations from emails bbelow will recieve this "
            "many free credits. (100 credits = $1.00) This is affects real "
            "money, use with care.",
        initial=0
    )
    emails = forms.CharField(widget=forms.Textarea,
        help_text="List of emails, separated by spaces, newlines or commas.")
    note = forms.CharField(widget=forms.Textarea,
        help_text="Admin use only: a reminder for yourself/others. "
            "e.g. Who was this issued to? Why?")

    def clean_emails(self):
        emails = re.split(r'[ \t\r\n,]+', self.cleaned_data['emails'])
        for email in emails:
            validate_email(email)
        if not emails:
            raise forms.ValidationError('Enter at least one email')
        return emails
