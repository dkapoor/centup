import random
import string

from django.db import models

class InviteCode(models.Model):
    CODE_LENGTH = 32
    code = models.CharField(
        max_length=CODE_LENGTH,
        unique=True,
        primary_key=True,
        default=lambda: ''.join(
            random.choice(
                string.letters + string.digits) for i in xrange(
                    0, InviteCode.CODE_LENGTH))
    )
    email = models.EmailField(max_length=254, null=True, blank=True,
        verbose_name="Tie to E-Mail",
        help_text="Make this invitation only usable for a user "
            "registering with this email address.")
    uses = models.PositiveIntegerField(default=1,
        help_text="Allow this code to be used multiple times. "
            "Probably doesn't make sense to increase this when also "
            "bound to a single email address.")
    issued_by = models.ForeignKey('users.User',
        related_name="issued_invites", editable=False)
    note = models.TextField(null=True, blank=True,
        help_text="Admin use only: a reminder for yourself/others. "
            "e.g. Who was this issued to? Why?")
    created_at=models.DateTimeField(auto_now_add=True, editable=False)
    bonus_credit_hc = models.PositiveIntegerField(default=0)

    @property
    def bonus_credit(self):
        return self.bonus_credit_hc / 2.0


    def __unicode__(self):
        return "Invite#%s" % self.code