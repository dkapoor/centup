# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'InviteCode'
        db.create_table(u'invites_invitecode', (
            ('code', self.gf('django.db.models.fields.CharField')(default='R4u4cP84Ln7ocrT3lyCovq99TgrywaHi', unique=True, max_length=32, primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254, null=True, blank=True)),
            ('uses', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('issued_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='issued_invites', to=orm['users.User'])),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'invites', ['InviteCode'])


    def backwards(self, orm):
        # Deleting model 'InviteCode'
        db.delete_table(u'invites_invitecode')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'balance_hc': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'charities.charity': {
            'Meta': {'object_name': 'Charity'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'charity'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'contact_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'cover': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'})
        },
        u'invites.invitecode': {
            'Meta': {'object_name': 'InviteCode'},
            'code': ('django.db.models.fields.CharField', [], {'default': "'k3VOyCxkujNHSmkLCHTpd0jqQjGhfTLN'", 'unique': 'True', 'max_length': '32', 'primary_key': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'issued_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'issued_invites'", 'to': u"orm['users.User']"}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'uses': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'user'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'auto_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'braintree_vault_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'charity_quote': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_donation_hc': ('django.db.models.fields.PositiveIntegerField', [], {'default': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invite': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invited_users'", 'to': u"orm['invites.InviteCode']"}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'notify_on_account_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_charity_message': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_low_balance': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'periodic_updates': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'preferred_charity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['charities.Charity']", 'null': 'True', 'blank': 'True'}),
            'preload_amount': ('django.db.models.fields.DecimalField', [], {'default': "'10.00'", 'max_digits': '19', 'decimal_places': '2', 'blank': 'True'}),
            'public_profile': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '254', 'blank': 'True'})
        }
    }

    complete_apps = ['invites']