from django.contrib import messages
from django.shortcuts import render, redirect

from social_auth.utils import setting

from invites.models import InviteCode
from invites.forms import BulkInviteForm
from invites.signals import requested_invite_notifiction

from django.http import HttpResponseRedirect
from mailchimp import utils

def bulk_add(request):
    if request.method == 'POST':
        form = BulkInviteForm(request.POST)
        if form.is_valid():
            for email in form.cleaned_data['emails']:
                invite = InviteCode(
                    issued_by=request.user,
                    email=email,
                    uses=1,
                    note=form.cleaned_data['note'] + "\n(generated from bulk-invite)",
                    bonus_credit_hc = form.cleaned_data['bonus_credit_hc']
                )
                invite.save()
                requested_invite_notifiction.send(sender=invite)
            messages.add_message(request, messages.INFO,
                "Sent out %s invites" % len(form.cleaned_data['emails']))
            form = BulkInviteForm()
        else:
            messages.add_message(request, messages.WARNING,
                "Problem with bulk invites")
    else:
        form = BulkInviteForm()

    return render(request, 'bulk_add.html', {'form':form})

def enter_invite_code(request):
    if request.method == 'POST' and request.POST.get('invite'):
        name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY', 'partial_pipeline')
        backend = request.session[name]['backend']
        code=request.POST['invite']
        error = None
        try:
            invite = InviteCode.objects.get(pk=code)
        except InviteCode.DoesNotExist:
            error = "This invitation code is invalid."
            invite = None
        if invite:
            if invite.uses < 1:
                error = "This invitation code has been used up."
            if invite.email:
                try:
                    email = request.session[name]['kwargs']['details']['email']
                except KeyError:
                    error = "This invitation code can only be used with a certain email address, however, your facebook account has not returned any email address to use."
                    email = None
                if email and invite.email != email:
                    error = "This invitation code is not valid for this email address."
        if error:
            return render(request, 'enter_invite_code.html',
                {'invite_error':error, 'code':code})
        else:
            request.session['saved_code'] = code
            return redirect('socialauth_complete', backend=backend)
    return render(request, 'enter_invite_code.html')


MAILCHIMP_LIST_ID = 'spamspamspamspameggsspamspam' # DRY :)
REDIRECT_URL_NAME = '/mailing_list_success/'

def add_email_to_mailing_list(request):
    if request.POST['email']:
        email_address = request.POST['email']
        list = utils.get_connection().get_list_by_id(MAILCHIMP_LIST_ID)
        list.subscribe(email_address, {'EMAIL': email_address})
        return HttpResponseRedirect('/mailing_list_success/')
    else:
        return HttpResponseRedirect('/mailing_list_failure/')



