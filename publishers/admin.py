from django.contrib import admin

from publishers import models
from utils.admin import AccountBalanceMixin

class PublisherAdmin(admin.ModelAdmin, AccountBalanceMixin):
    exclude = ('account', )
    list_display = ('name', 'credit_balance', 'dollar_balance', 'approved',
        'registered_by', 'url')
    list_filter = ('approved',)
    raw_id_fields = ("registered_by",)
    fieldsets = (
        ('General', {'fields': ('name', 'url','logo')}),
        ('Contact', {'fields': ('address1', 'address2', 'city', 'state', 'zipcode', 'country')}),
        ('About', {'fields': ('description', 'monthly_traffic','registered_by','paypal_email')}),
        ('Permissions', {'fields': ('featured', 'approved','hidden')}),
        )

admin.site.register(models.Publisher, PublisherAdmin)

class URLAdmin(admin.ModelAdmin):
    list_display = ('content', 'url', 'domain', 'publisher', 'total_donations')
    search_fields = ('title', 'url')

    def publisher(self, obj):
        return  obj.domain.publisher

admin.site.register(models.URL, URLAdmin)

class DomainAdmin(admin.ModelAdmin):
    list_display = ('domain', 'publisher')
admin.site.register(models.Domain, DomainAdmin)
