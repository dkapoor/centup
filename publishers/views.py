from datetime import datetime, timedelta

from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from users.models import User
from publishers import forms
from publishers.models import Publisher, URL

DEFAULT_EARNERS_WINDOW=timedelta(days=90)

@login_required
def dash(request, publisher_id):
    publisher = get_object_or_404(Publisher, pk=publisher_id)
    if not request.user.may_see_dashboard(publisher):
        raise PermissionDenied("This isn't yours.")
    data_since = datetime.now() - DEFAULT_EARNERS_WINDOW
    return render(request, 'publisher/dash.html', dict(
        publisher=publisher,
        top_earners=_iter_top_earners(
            publisher.top_earners(since=data_since)
        ),
        top_givers=_iter_top_givers(
            publisher.top_givers(since=data_since)
        )
    ))

def publishers_landing(request):
    data_since = datetime.now() - DEFAULT_EARNERS_WINDOW
    return render(request, 'publisher/publishers.html', dict(
        publishers=Publisher.objects.filter(approved="TRUE").order_by('name'),
    ))


def _iter_top_earners(qs):
    for earner in qs:
        yield URL.objects.get(pk=earner['url']), earner['earnings_hc'] / 2.0

def _iter_top_givers(qs):
    for giver in qs:
        yield User.objects.get(account=giver['source_account']), giver['earnings_hc'] / 2.0

@login_required
def signup(request):
    if request.method == 'POST':
        form = forms.PublisherSignUpForm(request.POST, request.FILES)
        if form.is_valid():
            publisher = form.save(commit=False)
            publisher.registered_by = request.user
            publisher.approved = False
            publisher.save()
            messages.info(request, "Thank you for signing up - we will contact you after we review your application.")
            return HttpResponseRedirect(reverse('users:dash'))
    else:
        form = forms.PublisherSignUpForm()
    return render(request, "publisher/signup.html", dict(
        form=form
    ))