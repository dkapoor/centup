# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

def credits(dec):
    return int((dec * 200).to_integral_exact())

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        for url in orm['publishers.URL'].objects.all():
            url.total_donations_hc = credits(url.total_donations)
            url.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'balance': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '19', 'decimal_places': '10'}),
            'balance_hc': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'charities.charity': {
            'Meta': {'object_name': 'Charity'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'charity'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'contact_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'publishers.domain': {
            'Meta': {'object_name': 'Domain'},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'domains'", 'to': u"orm['publishers.Publisher']"})
        },
        u'publishers.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'publisher'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'paypal_email': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'registered_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']", 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'publishers.url': {
            'Meta': {'object_name': 'URL'},
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'urls'", 'to': u"orm['publishers.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'total_donations': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '19', 'decimal_places': '10'}),
            'total_donations_hc': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'user'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'auto_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'braintree_vault_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'charity_quote': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_donation_hc': ('django.db.models.fields.PositiveIntegerField', [], {'default': '50'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'notify_on_account_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_charity_message': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_low_balance': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'periodic_updates': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'preferred_charity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['charities.Charity']", 'null': 'True', 'blank': 'True'}),
            'preload_amount': ('django.db.models.fields.DecimalField', [], {'default': "'0.50'", 'max_digits': '19', 'decimal_places': '10', 'blank': 'True'}),
            'public_profile': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '254', 'blank': 'True'})
        }
    }

    complete_apps = ['publishers']
    symmetrical = True
