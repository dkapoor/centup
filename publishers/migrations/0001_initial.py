# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ("users", "0001_initial"),
    )

    def forwards(self, orm):
        # Adding model 'Publisher'
        db.create_table(u'publishers_publisher', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('address1', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('address2', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('paypal_email', self.gf('django.db.models.fields.CharField')(max_length=128, blank=True)),
            ('registered_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.User'], null=True, blank=True)),
            ('approved', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'publishers', ['Publisher'])

        # Adding model 'Domain'
        db.create_table(u'publishers_domain', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('domain', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('publisher', self.gf('django.db.models.fields.related.ForeignKey')(related_name='domains', to=orm['publishers.Publisher'])),
        ))
        db.send_create_signal(u'publishers', ['Domain'])

        # Adding model 'URL'
        db.create_table(u'publishers_url', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('domain', self.gf('django.db.models.fields.related.ForeignKey')(related_name='urls', to=orm['publishers.Domain'])),
            ('centup_balance', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=19, decimal_places=10)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
        ))
        db.send_create_signal(u'publishers', ['URL'])


    def backwards(self, orm):
        # Deleting model 'Publisher'
        db.delete_table(u'publishers_publisher')

        # Deleting model 'Domain'
        db.delete_table(u'publishers_domain')

        # Deleting model 'URL'
        db.delete_table(u'publishers_url')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'balance': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '19', 'decimal_places': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_fee_account': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'charities.charity': {
            'Meta': {'object_name': 'Charity'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'charity'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'contact_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'publishers.domain': {
            'Meta': {'object_name': 'Domain'},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'domains'", 'to': u"orm['publishers.Publisher']"})
        },
        u'publishers.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'paypal_email': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'registered_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.User']", 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'publishers.url': {
            'Meta': {'object_name': 'URL'},
            'centup_balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '19', 'decimal_places': '10'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'urls'", 'to': u"orm['publishers.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        u'users.user': {
            'Meta': {'object_name': 'User'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'user'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'auto_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'braintree_vault_token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'charity_quote': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'default_centup': ('django.db.models.fields.DecimalField', [], {'default': "'0.25'", 'max_digits': '19', 'decimal_places': '10', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '254'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'notify_on_account_reload': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_charity_message': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'notify_on_low_balance': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'periodic_updates': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'preferred_charity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['charities.Charity']", 'null': 'True', 'blank': 'True'}),
            'preload_amount': ('django.db.models.fields.DecimalField', [], {'default': "'0.50'", 'max_digits': '19', 'decimal_places': '10', 'blank': 'True'}),
            'public_profile': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '254', 'blank': 'True'})
        }
    }

    complete_apps = ['publishers']