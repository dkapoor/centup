from django import forms

from publishers import models

class PublisherSignUpForm(forms.ModelForm):
    """Edit form for the admin"""

    class Meta:
        model = models.Publisher
        fields = ('name', 'url', 'logo', 'address1', 'address2', 'city',
            'state', 'zipcode', 'country', 'paypal_email','monthly_traffic')

