from urlparse import urlparse
from django.conf import settings
from django.db import models
from django.db.models import Sum


class Publisher(models.Model):
    name = models.CharField(max_length=128)
    url = models.URLField(blank=True)
    logo = models.ImageField(upload_to='logos/publishers/', blank=True)
    address1 = models.CharField(max_length=128, blank=True)
    address2 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=128, blank=True)
    state = models.CharField(max_length=128, blank=True)
    zipcode = models.CharField(max_length=5, blank=True)
    country = models.CharField(max_length=128, blank=True)
    paypal_email = models.CharField(max_length=128, blank=True)
    account = models.OneToOneField('accounts.Account', related_name='publisher')
    registered_by = models.ForeignKey('users.User', blank=True, null=True, related_name='publishers')
    approved=models.BooleanField(default=False)
    description = models.TextField(blank=True)
    monthly_traffic = models.CharField(max_length=128, blank=True)
    featured=models.BooleanField(default=False)
    hidden=models.BooleanField(default=False)
    


    def __unicode__(self):
        return self.name

    def top_earners(self, since=None):
        donations = self.account.publisher_donation_sources
        donations = donations.values('url').annotate(earnings_hc=models.Sum('publisher_amount_hc'))
        if since:
            donations = donations.filter(created_at__gte=since)
        return donations.order_by('-earnings_hc')

    def top_givers(self, since=None):
        donations = self.account.publisher_donation_sources
        donations = donations.values('source_account').annotate(earnings_hc=models.Sum('publisher_amount_hc'))
        if since:
            donations = donations.filter(created_at__gte=since)
        return donations.order_by('-earnings_hc')


    #Number of donations
    #def donation_count(self):
    #donations = self.account.publisher_donation_sources
    #donations = donations.values('publisher_amount_hc')
    #return len(donations)

    #Amount donated by publisher
    #def donation_tally(self):
    #donations = self.account.publisher_donation_sources
    #donations = donations.aggregate(donations_tally=models.Sum('publisher_amount_hc'))
    #return donations.self      
            
class Domain(models.Model):
    domain = models.CharField(max_length=256)
    publisher = models.ForeignKey(Publisher, related_name='domains')

    def __unicode__(self):
        return self.domain

class URL(models.Model):
    # see http://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url
    url = models.CharField(max_length=2000)
    domain = models.ForeignKey(Domain, related_name='urls')
    total_donations_hc = models.PositiveIntegerField(default=0)

    title = models.CharField(max_length=256, blank=True)

    @property
    def total_donations(self):
        return self.total_donations_hc / 2.0

    @property
    def content(self):
        return self.title or self.url

    def __unicode__(self):
        return self.url

    class MissingDomain(Exception):
        pass
    class UnclaimedDomain(Exception):
        pass

    def register_donation_amount(self, amount):
        amount_hc = amount * 2
        self.__class__.objects.filter(pk=self.pk).update(
            total_donations_hc=models.F('total_donations_hc') + amount_hc
        )
        # also update cached reference
        self.total_donations_hc += amount_hc


    @classmethod
    def from_string(cls, url_string, title=''):
        """Find or create the URL record based on the normalized `url_string`"""
        parsed_url = urlparse(url_string)
        if not parsed_url.hostname:
            parsed_url = urlparse('http://' + url_string)
        if not parsed_url.hostname:
            raise URL.MissingDomain()
        try:
            domain = Domain.objects.get(domain=parsed_url.hostname)
        except Domain.DoesNotExist:
            raise URL.UnclaimedDomain(parsed_url.hostname)
        url, created = cls.objects.get_or_create(
            url=parsed_url.geturl(),
            defaults=dict(
                domain=domain,
                title=title
            )
        )
        if not created and title and url.title != title:
            # Update title, if given and different
            url.title = title
            url.save()
        return url
