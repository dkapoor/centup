from django.conf.urls import patterns, include, url

specific = patterns('publishers.views',
    url(r'^dash/?$', 'dash', name='dash')
)

collection = patterns('publishers.views',
    url(r'^signup/?$', 'signup', name='signup'),
    url(r'^/?$', 'publishers_landing', name='landing')
)

urlpatterns = patterns('',
    url(r'^/(?P<publisher_id>\d+)/', include(specific, namespace='publisher')),
    url(r'^/', include(collection, namespace='publishers')),
)