from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from accounts.signals import low_balance, account_reload
from invites.signals import requested_invite_notifiction
from news.models import Post
from notifications.mail import send_notification
from users.models import User
from publishers.models import Publisher



@receiver(post_save, sender=User)
def send_welcome(sender, created, instance, raw, **kwargs):
    if created and not raw:
        send_notification(instance, "notifications/welcome.txt")


# do we need forgot password? that's probably hadnled by contrib.auth


@receiver(low_balance)
def send_low_balance(sender, **kwargs):
    if sender.user.notify_on_low_balance and not sender.user.auto_reload:
        send_notification(sender.user,
            "notifications/low_balance.txt", **kwargs)

@receiver(account_reload)
def send_account_reload(sender, **kwargs):
    if sender.notify_on_account_reload:
        send_notification(sender, "notifications/account_reload.txt", **kwargs)

# What does this hook up ?
def send_charity_message(sender, **kwargs):
    if user.notify_on_charity_message:
        send_notification

# Publisher notification email. 

#def publisher_approved(sender, **kwargs):
    #if publisher.approved:
        #send_notification(sender, "notifications/publisher_approval_msg.txt", **kwargs)



# News item receiver. Commenting out
# @receiver(post_save, sender=Post)
# def send_periodic_update(sender, created, instance, raw, **kwargs):
#    recipients = list(User.objects.filter(periodic_updates=True))
#    send_notification(recipients, "notifications/periodic_updates.txt", post=instance)

@receiver(requested_invite_notifiction)
def send_invitation(sender, **kwargs):
    send_notification(sender, "notifications/invitation.html")