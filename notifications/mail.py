import logging
logger = logging.getLogger(__name__)
import re
import mimetypes

from django.conf import settings
from django.core.mail import send_mass_mail, EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from email.mime.text import MIMEText


def send_notification(recipient, template, **variables):
    """Send a notification to `recipient` User, using provided `template`,
    which will be loaded with given `varibales`.

    Also accepts list of `recipient`s which will send a mail to each."""
    if isinstance(recipient, list):
        recipients = recipient
    else:
        recipients = [recipient]

    variables['root_url'] = settings.PUBLIC_URL_ROOT
    messages = []
    for recipient in recipients:
        variables['recipient'] = recipient
        message = get_template(template).render(Context(variables))
        subject, html_content = re.split(r'\n+', message.strip(), maxsplit=1)
        
        logger.debug("From: %s", settings.NOTIFACTION_FROM_ADDRESS)
        logger.debug("To: %s", recipient.email)
        logger.debug("Subject: %s", subject)
        logger.debug('------body------\n%s', html_content)

        for message in messages:
            message = EmailMultiAlternatives(html_content)
            message = MIMEText(html_content, 'html')
            message.content_subtype = "html"

        messages.append((
            subject,
            html_content,
            settings.NOTIFACTION_FROM_ADDRESS,
            [recipient.email]))

        

    send_mass_mail(messages, fail_silently=False)
    logger.info("Sent %r email to %s" % (template, ', '.join(r.email for r in recipients)))